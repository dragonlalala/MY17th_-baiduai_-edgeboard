import os
import sys
sys.path.append("/home/root/workspace/17BaiduAI_V1") 

from Modules.Vision.Camera import frontCamObj
from Modules.Vision.Cruise import cruiseObj


if __name__ == "__main__":
    frontCamObj.start()
    cruiseObj.start()
