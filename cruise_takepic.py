#!/usr/bin/python
# coding=UTF-8
import sys
import time
# from Modules.Config import logging
# from Modules.Config import IS_FRIENDSHIP_OPENLOOP
# from Modules.Config import timeout_dict
# from Modules.Config import sign_threshold
# from Modules.Config import sign_state
# from Modules.Config import fruit_y_thresh
# from Modules.Cruise.CruiseCpp import *
from Modules.Cruise.CruiseModel import *
from Modules.Vision.ModelTest import *
from Modules.Serial.Serial import serialObj  
import Modules.Watch.Watch

"""
camera cruise detect 开启进程
"""

frontCamObj.start()
time.sleep(0.1)
cruiseObj.start()
signDetectObj.start()
"""
全局变量
"""
bad_target_num = 0  #友谊放歌标号，识别后置一不再进入该模型
flag_slow = True      # 用于防止多次进入发送在地标前减速的if里面
if not os.path.exists('./d1'):
    os.makedirs('./d1') #创建路径

if __name__ == "__main__":
    sign_index = 0
    sign_index_list = []
    start_time = time.time()
    stop_time = start_time + 1000 # 初始化地标前停车的时间戳
    logging.info("start_time:{}".format(start_time))
    stop_distance = 0
    Modules.Watch.Watch.Watcher() # 随时响应 Ctrl+c 的键盘输入
    serialObj.writeData([0xAA, 0xDE, 250, 100, 100, 0xFF]) 
    while 1:
        if signDetectObj.sign_index.value == 6 and flag_slow:
            flag_slow = False
            serialObj.writeData([0xAA, 0xDE, 100+50, 100, 100, 0xFF])
            logging.info("send speed slow at seesaw")
        # 每种地标对应的任务只做一次;时间戳达到停车的时间;因为time.time一开始很大;地标检测仍开启(针对以物易物);跷跷板
        if signDetectObj.sign_index.value == 6 and signDetectObj.sign_y.value > 150:
            signDetectObj.pause()

        if signDetectObj.getPausedFlag():
            # serialObj.writeData([0xAA, 0xDE, 100, 100, 100, 0xFF])
            cruiseObj.sendFlag.value = False
            # serialObj.writeData([0xAA, 0xC1, 100, 100, 100+0, 0xFF])
            # serialObj.writeData([0xAA, 0xDE, 150, 100, 100, 0xFF])
            time.sleep(1.3)
            serialObj.writeData([0xAA, 0xDE, 250, 100, 100, 0xFF])
            cruiseObj.sendFlag.value = True
            logging.info("cruiseObj.sendFlag.value:{}".format(cruiseObj.sendFlag.value))
            flag_slow = True
            signDetectObj.resume()
            time.sleep(0.1)
            continue