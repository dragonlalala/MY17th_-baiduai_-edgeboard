#!/usr/bin/python
# coding=UTF-8
import sys
import time
from Modules.Camera.CameraMulti import frontCamObj
from Modules.Cruise.CruiseModel import *
from Modules.Serial.Serial import serialObj
from Modules.Config import logging
"""
camera cruise detect 开启进程
"""

frontCamObj.start()
time.sleep(0.1)
if not os.path.exists('./d1'):
    os.makedirs('./d1') #创建路径

count = 0
img_count = 23467
serialObj.writeData([0xAA, 0xDE, 250, 100, 100, 0xFF])
while True:
    front_image = frontCamObj.readForDetect()
    if front_image is not None:
        start = time.time()
        angle = cruiseObj.infer_cnn(front_image)
        angle = angle*100
        logging.info("time{}".format(time.time()-start))
        logging.info("mid_error:{}".format(int(angle)))
        if -100 <= angle <=100:
            serialObj.writeData([0xAA,0xC1,100,100,int(angle+100),0xFF])
        count += 1
        # if count % 5 == 0:
        #     img_count += 1
        #     cv2.imwrite('d1/'+str(img_count)+'.jpg',front_image)
        #     imgpath = 'd1/{}.jpg'.format(img_count)
        #     with open('d1.txt', 'a') as f:
        #         f.writelines(imgpath + '\t' + str(angle/100) + '\n')