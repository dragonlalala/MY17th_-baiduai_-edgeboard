#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <iostream>
#include <ctime>
using namespace std;
using namespace cv;

int main() {
	cv::Mat srcImage = imread("1.jpg");
	clock_t startTime, endTime;
	startTime = clock();
	if (!srcImage.data)
		return -1;
	// imwrite("srcImage.jpg", srcImage);
	//将图片按比例缩放至宽为250像素的大小
	int nRows = 300;
	int nCols = 300;

	Mat img1(nRows, nCols, srcImage.type());
    Mat dst;
    cvtColor(img1,dst,COLOR_BGR2RGB);
	resize(srcImage, dst, img1.size(), 0, 0, INTER_LINEAR);
    dst.convertTo(dst, CV_32F);
    Mat dst1;
    dst -=127.5;
    dst*=0.07843;
	// normalize(dst, dst1, 127.5, 255.0, NORM_MINMAX);
	endTime = clock();//计时结束
	cout << "t:" << (double)(endTime - startTime) / 1000 << endl;
	imwrite("dst.jpg", dst);


	// waitKey(0);
	return 0;
}