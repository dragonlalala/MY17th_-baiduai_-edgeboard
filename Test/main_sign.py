'''
Author: your name
Date: 2022-04-07 11:50:18
LastEditTime: 2022-04-28 10:41:27
LastEditors: Please set LastEditors
Description: In User Settings Edit
FilePath: \17th_BAIDUAI_V1\main.py
'''
import time
import logging
from Modules.Vision.Cruise import cruiseObj
from Modules.Vision.Camera import frontCamObj
from Modules.Vision.Camera import leftCamObj
from Modules.Vision.Camera import rightCamObj
from Modules.Vision.ModelTest import signDetectObj
from Modules.Vision.ModelTest import taskDetectObj
from Modules.Serial.Serial import serialObj
import Modules.Watch.Watch
"""
camera cruise detect 开启线程
"""
frontCamObj.start()
cruiseObj.start()
signDetectObj.start()
leftCamObj.start()
leftCamObj.pause()
rightCamObj.start()
rightCamObj.pause()
"""
全局变量
"""
flag_slow = True  # 用于防止多次进入发送在地标前减速的if里面
left_cam_sign = [1,2,3] # 记录需要开启左摄像头的地标index
"""
main loop
"""

if __name__ == "__main__":
    Modules.Watch.Watch.Watcher() # 随时响应 Ctrl+c 的键盘输入
    while 1:
        if signDetectObj.sign_y > 45 and 95 < signDetectObj.sign_x < 250 and flag_slow:
            flag_slow = False
            # 发送减速指令给下位机
            serialObj.writeData([0xAA, 0xDE, 1, 2200 // 10, 0, 0xFF])

        if signDetectObj.getPausedFlag():
            if signDetectObj.sign_index in left_cam_sign: # 开左摄像头
                leftCamObj.resume()
                while True:
                    try:
                        if taskDetectObj.testTask(2,2): # 侧面检测模型开始，第一个2表示传进去左摄像头,第二个2表示不resize图片
                            if taskDetectObj.x < 160:
                                # 发送停车指令给下位机
                                serialObj.writeData([0xAA, 0xDE, 1, 2200 // 10, 0, 0xFF])
                                cruiseObj.pause() # 暂停巡航
                                frontCamObj.pause() # 暂时关闭前摄像头
                                break
                            else: pass
                    except Exception as e:
                        # logging.error("raise an error at RESULT_INDEX_detect.value == 2 in main")
                        # logging.error(e)  # 打印错误
                        continue
            else: #开右摄像头
                rightCamObj.resume()
                while True:
                    try:
                        if taskDetectObj.testTask(1,2): # 侧面检测模型开始，第一个1表示传进去右摄像头,第二个2表示不resize图片
                            if taskDetectObj.x < 160:
                                # 发送停车指令给下位机
                                serialObj.writeData([0xAA, 0xDE, 1, 2200 // 10, 0, 0xFF])
                                cruiseObj.pause() # 暂停巡航
                                frontCamObj.pause() # 暂时关闭前摄像头
                                break
                            else: pass
                    except Exception as e:
                        # logging.error("raise an error at RESULT_INDEX_detect.value == 2 in main")
                        # logging.error(e)  # 打印错误
                        continue
                

        # 发送对应任务指令给下位机，开始做任务！
        if cruiseObj.getPausedFlag() and signDetectObj.sign_index == 1: # 1表示宿营
            serialObj.writeData([0xAA, 0xDE, 1, 2200 // 10, 0, 0xFF])
            while True:  # 等待下位机完成任务
                logging.debug('wait TC264 send finish task flag index1 in main')
                if serialObj.readData() == 'af':
                    logging.debug('**********receive af at index1**********')
                    break
            leftCamObj.pause()
            time.sleep(0.1)  # 延时一会，等待 left_cam 完全停止
            frontCamObj.resume()
            time.sleep(0.1)
            cruiseObj.resume()
            flag_slow = True
            signDetectObj.resume()

        elif cruiseObj.getPausedFlag() and signDetectObj.sign_index == 2: # 2表示夹方块
            serialObj.writeData([0xAA, 0xDE, 1, 2200 // 10, 0, 0xFF])
            while True:  # 等待下位机完成任务
                logging.debug('wait TC264 send finish task flag index2 in main')
                if serialObj.readData() == 'af':
                    logging.debug('**********receive af at index2**********')
                    break
            leftCamObj.pause()
            time.sleep(0.1)  # 延时一会，等待 left_cam 完全停止
            frontCamObj.resume()
            time.sleep(0.1)
            cruiseObj.resume()
            flag_slow = True
            signDetectObj.resume()

        elif cruiseObj.getPausedFlag() and signDetectObj.sign_index == 3: # 3表示举旗
            serialObj.writeData([0xAA, 0xDE, 1, 2200 // 10, 0, 0xFF])
            while True:  # 等待下位机完成任务
                logging.debug('wait TC264 send finish task flag index3 in main')
                if serialObj.readData() == 'af':
                    logging.debug('**********receive af at index3**********')
                    break
            leftCamObj.pause()
            time.sleep(0.1)  # 延时一会，等待 left_cam 完全停止
            frontCamObj.resume()
            time.sleep(0.1)
            cruiseObj.resume()
            flag_slow = True
            signDetectObj.resume()

        elif cruiseObj.getPausedFlag() and signDetectObj.sign_index == 4: # 4表示倒小球
            serialObj.writeData([0xAA, 0xDE, 1, 2200 // 10, 0, 0xFF])
            while True:  # 等待下位机完成任务
                logging.debug('wait TC264 send finish task flag index4 in main')
                if serialObj.readData() == 'af':
                    logging.debug('**********receive af at index4**********')
                    break
            rightCamObj.pause()
            time.sleep(0.1)  # 延时一会，等待 left_cam 完全停止
            frontCamObj.resume()
            time.sleep(0.1)
            cruiseObj.resume()
            flag_slow = True
            signDetectObj.resume()

        elif cruiseObj.getPausedFlag() and signDetectObj.sign_index == 5: # 5表示打靶
            serialObj.writeData([0xAA, 0xDE, 1, 2200 // 10, 0, 0xFF])
            while True:  # 等待下位机完成任务
                logging.debug('wait TC264 send finish task flag index5 in main')
                if serialObj.readData() == 'af':
                    logging.debug('**********receive af at index5**********')
                    break
            rightCamObj.pause()
            time.sleep(0.1)  # 延时一会，等待 left_cam 完全停止
            frontCamObj.resume()
            time.sleep(0.1)
            cruiseObj.resume()
            flag_slow = True
            signDetectObj.resume()