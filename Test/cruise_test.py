'''
Author: your name
Date: 2022-02-22 21:49:18
LastEditTime: 2022-03-16 10:15:04
LastEditors: Please set LastEditors
Description: In User Settings Edit
FilePath: \roadtrace\lab3V2.py
'''
#!/usr/bin/python3
# -*- coding: <utf-8> -*-
import cv2
import numpy as np
# from numba import jit
# from numba.typed import list
import time


ROW = 60 # 行
COL = 80 # 列
BLACK = 0
WHITE = 255
MIDDLE = COL//2 # 中间值


'''
图像处理相关，这里不使用类是为了之后使用numba、调用.so文件做准备
'''
cap = cv2.VideoCapture(0)
oriImg = None
# 二值化图像
img = None
lower = np.array([19,83,116])
upper = np.array([65,165,245])

'''
寻线相关的全局变量
'''
#十字标志  0(无)   1(全十字)   2(上半十字)    3(只有一个拐点)
cross_flag = 0   

# 弯道标志 
turn_flag =0    

# 左右方向的标志
LEFT_DIR = 1111  
RIGHT_DIR = 1000

# 左右边界的有效行列表，取第一个元素为有效行
valid_row = 40   #！！！最远有效行初始值可调
left_valid_row = list()
right_valid_row = list()
left_valid_row.append(1)
right_valid_row.append(1)
left_valid_row.clear()
right_valid_row.clear()

left_finallosey = 0
right_finallosey = 0

left_boundary = np.zeros((ROW),dtype=np.int32)
right_boundary = np.zeros((ROW),dtype=np.int32)
mid_line = np.zeros((ROW),dtype=np.int32)

# 记录左右丢线的时的下标
right_loss = list()
left_loss = list()



# 需要指明数据的类型，给编译器辨识
right_loss.append(1)
left_loss.append(1)
right_loss.clear()
left_loss.clear()


def imgProcess():
    '''
    description: 处理图片得到二值化图片
    param {*}
    return {*}
    '''    
    global img
    img_hsv = cv2.cvtColor(oriImg,cv2.COLOR_BGR2HSV)
    # ！！！图像HSV阈值可以再调精确些
    mask = cv2.inRange(img_hsv,lower,upper)
    # TODO:实际可能需要降噪措施。
    img = cv2.resize(mask,(COL,ROW),interpolation=cv2.INTER_NEAREST)
    element = cv2.getStructuringElement(cv2.MORPH_RECT, (3, 3))
    imgdilate = cv2.dilate(img, element)  # 膨胀
    img = cv2.morphologyEx(imgdilate, cv2.MORPH_CLOSE, element)  # 先腐蚀后膨胀操作，原为腐蚀操作
    


# @jit(nopython=True)
def leftFindChange(img,tmp_y, left_loss = left_loss,left_valid = left_valid_row
                ,left_boundary = left_boundary, start_x=MIDDLE):
    # 取特定的行
    img_row = img[tmp_y,:]
    left_have_found = 0  # 用于判断是否找到黑白边界

    for x in range(start_x, 0, -3):
        if img_row[x] == 255:
            if img_row[x + 1] == 0:
                left_boundary[tmp_y] = x
                left_have_found = 1
                break
            elif img_row[x + 2] == 0:
                left_boundary[tmp_y] = x + 1
                left_have_found = 1
                break
            elif img_row[x + 3] == 0:
                left_boundary[tmp_y] = x + 2
                left_have_found = 1
                break
            else:
                # 最远有效行
                left_valid.append(tmp_y)
    if left_have_found == 0:
        left_boundary[tmp_y] = 0  # 找不到跳变点时赋予0
        left_loss.append(tmp_y)
    return left_boundary[tmp_y]  # 返回最后加入的那个


# @jit(nopython=True)
def rightFindChange(img,tmp_y, right_loss=right_loss, right_value=right_valid_row
                ,right_boundary=right_boundary, start_x=MIDDLE,  x_max=COL):
    img_row = img[tmp_y,:]
    right_have_found = 0  # 用于判断是否找到黑白边界

    for x in range(start_x, x_max, 3):  # 3个点跳着找，速度快
        if img_row[x] == 255: 
            if img_row[x - 1] == 0:
                right_boundary[tmp_y] = x
                right_have_found = 1
                break
            elif img_row[x - 2] == 0:
                right_boundary[tmp_y] = x - 1
                right_have_found = 1
                break
            elif img_row[x - 3] == 0:
                right_boundary[tmp_y] = x - 2
                right_have_found = 1
                break
            else:
                # 最远有效行
                right_value.append(tmp_y)   
    if right_have_found == 0:
        right_boundary[tmp_y] = x_max - 1  # 找不到跳变点时赋予图像的最大长度-1
        right_loss.append(tmp_y)
    return right_boundary[tmp_y]

# @jit(nopython=True)
def Get_LR_Change_Points(img, y_min=40, y_max=ROW,start_x = MIDDLE,
                right_loss=right_loss,right_boundary=right_boundary,
                left_loss= left_loss, left_boundary=left_boundary,mid_line=mid_line,
                left_valid_row = left_valid_row,right_valid_row = right_valid_row,):
    """

    :param img:二值化图像
    :param y_min: 设置有效行的终点
    :param y_max: 设置有效行的起点
    :param start_x 给定一个开始的查找起点
    :return: 得到左右跳变点的数组  
    """
    right = left = 0
    right = rightFindChange(img, tmp_y=y_max-1, right_loss=right_loss,right_value=right_valid_row,
                            right_boundary=right_boundary, start_x=start_x)
    left = leftFindChange(img, tmp_y=y_max-1,  left_loss= left_loss, left_valid=left_valid_row,
                            left_boundary=left_boundary, start_x=start_x)
    mid = (right+left)//2
    mid_line[y_max-1] = mid
    for y in range(y_max - 2, y_min - 1, -1):
        right = rightFindChange(img, tmp_y=y, right_loss=right_loss,right_value=right_valid_row,
                                right_boundary=right_boundary, start_x=mid)
        left = leftFindChange(img, tmp_y=y,  left_loss= left_loss,  left_valid=left_valid_row,
                                left_boundary=left_boundary, start_x=mid)
        mid_line[y] = (right+left)//2


def getLastLoss():
    '''
    description: 获得左右两边的最终丢线行
    param {*}
    return {*}
    '''    
    global left_finallosey, right_finallosey
    global left_loss,right_loss
    #求两边最后丢线行   
    if len(left_loss)  > 1:
            left_finallosey = left_loss[-1]
   
    if len(right_loss) > 1:
        right_finallosey = right_loss[-1]








def drawImg(left_boundary,right_boundary,mid_line,img):
        """
        在图片上画左中右线
        :param left_boundary:
        :param right_boundary:
        :param mid_line:
        :param img:
        :return:
        """
        try:
            for i in range(ROW - 1, 19, -1):
                img[i][left_boundary[i] + 3] = 255
            for i in range(ROW - 1, 19, -1):
                img[i][right_boundary[i] - 3] = 255
            for i, mid in enumerate(mid_line):
                img[i][mid] = 255
        except Exception as e:
            print(e)

def MidErrorCal(mid_line = mid_line):
    """
    得到中线的偏差
    :return:
    """
    global valid_row
    mid_error = (sum(mid_line[(valid_row): ]) - MIDDLE * (ROW-valid_row)) // (ROW-valid_row)  
    # mid_error小于0时，车子是左偏的，要向右矫正
    # 偏差限幅
    if mid_error > 100:
        mid_error = 100
    elif mid_error < -100:
        mid_error = -100
    return mid_error


def Find_valid_row(left_valid_row = left_valid_row ,right_valid_row = right_valid_row):
    """
    比较得到最远有效行
    两个list初始为空
    """
    global valid_row
    L_len = len(left_valid_row)
    R_len = len(right_valid_row)
    if  L_len==0 and R_len>0:
        valid_row = right_valid_row[0]
    if L_len >0 and R_len==0:
        valid_row = left_valid_row[0]
    if L_len >0 and R_len>0:
        if left_valid_row[0]<right_valid_row[0]:
            valid_row = left_valid_row[0]
        else:
            valid_row = right_valid_row[0]



# 识别元素 =============================================================================        
def repairLine(down_y, down_x, up_y, up_x, direction, 
                left_boundary = left_boundary, right_boundary = right_boundary):    
    '''
    description: 主要用于十字路口补线    
    param {down_y, down_x, up_y, up_x, direction} 上下拐点和方向
    return {*}
    '''   
    # 左    
    if  direction == LEFT_DIR:          
        k = (up_x - down_x)/(down_y - up_y)
        for i in range(down_y-1 , up_y , -1):
            left_boundary[i] = down_x + int(k*(down_y-i))
            #print(left_boundary[down_y-i])
    # 右                         
    else:                                
        k = (down_x - up_x)/(down_y - up_y)
        for i in range(down_y-1 , up_y , -1):
            right_boundary[i] = down_x - int(k*(down_y-i))
            #print(right_boundary[down_y-i])
            

def findNineSquares(i,j,img = img):
    '''
    description: 遍历拐点周围九宫格
    param {i,j} 拐点位置
    return {count} 拐点周围白色点的数量
    '''    
    count =0
    for k in range(3):          
        for l in range(3):
            if img[i-1+k][j-1+l] == 255:
                count = count + 1
    return count


def crossReg(mid_line = mid_line,left_boundary = left_boundary,
             right_boundary = right_boundary):   
    '''
    description: 识别十字
    param {cross_flag} 十字标志
    return {*}
    '''            
    global cross_flag                   
    # 四个拐点标志
    R_down_flag =0      
    R_up_flag = 0
    L_down_flag = 0
    L_up_flag = 0
    
    
    # 找右下拐点
    i = 0
    for i in range(ROW-1, 30 ,-1):           #TODO:！！！参数(60)可调   从下往上遍历
        if right_boundary[i-1] > right_boundary[i] and right_boundary[i-2] >= right_boundary[i-1] and right_boundary[i-2] - right_boundary[i]>3 and i>=valid_row+2:   #参数（3）可调 
            j = right_boundary[i] 
            count = findNineSquares(i,j,img)
            if count >=3:        #TODO:!!!参数要调
                R_down_point = np.array([i,j])     #！！！行，列 ==> y,x
                R_down_flag=1
                break
    
    #找右上拐点
    i-=1               
    if i<=36:     #TODO:参数可调
        Now_i =ROW -1
    else:
        Now_i =i
    for i in range(Now_i,20,-1):    
        if right_boundary[i] - right_boundary[i-1] >=5 and i>=valid_row+3 :
            if right_boundary[i-2] - right_boundary[i-3] < 3 and right_boundary[i-1] - right_boundary[i-2] < 3: 
                j = right_boundary[i-1]
                count = findNineSquares(i-1,j,img)
                #print(count)
                if count>=3:
                    R_up_point = np.array([i-1,j])
                    R_up_flag=1
                        
                    # print("右上",j,i-1)
                    break
        
    # 找左下拐点
    i = ROW-1
    for i in range(ROW-1, 30 ,-1):           #！！！参数(60)可调   从下往上遍历
        if left_boundary[i-1] <left_boundary[i] and left_boundary[i-2] <= left_boundary[i-1] and left_boundary[i] - left_boundary[i-2] >3 and i>=valid_row+2:
            j =left_boundary[i]       
            count = findNineSquares(i,j,img)
            if count >=3:
                L_down_point =np.array([i,j])     #！！！行，列 ==> y,x
                #print(L_down_point)
                L_down_flag =1
                break   

    # 找左上拐点  
    i -=1
    if i<=36:
        Now_i =ROW -1
    else:
        Now_i =i
    for i in range(Now_i,20,-1):      
        if left_boundary[i-1] - left_boundary[i] >=5 and i>=valid_row+3:
            #print(left_boundary[i-3] - left_boundary[i-2])
            #print(left_boundary[i-2] - left_boundary[i-1])
            if left_boundary[i-3] - left_boundary[i-2] < 3 and left_boundary[i-2] - left_boundary[i-1] <3:
                j = int(left_boundary[i-1])
                count = findNineSquares(i-1,j,img) 
                #print(count)
                if count>=3:
                    L_up_point = np.array([i-1,j]) 
                    L_up_flag=1
                    # print("左上",j,i-1)
                    break
                    
                
    # print('右下 右上 左下 左上')            
    # print(R_down_flag ,
    #       R_up_flag ,
    #       L_down_flag ,
    #       L_up_flag )
   

    #全十字
    if L_down_flag and L_up_flag and R_down_flag  and R_up_flag :
        if L_down_point[1] < R_up_point[1] and L_up_point[1] < R_down_point[1]:     #横坐标交叉比较
            if L_down_point[0] > R_up_point[0] and L_up_point[0] < R_down_point[0]: #纵坐标交叉比较
                count_Black = count = 0
                points = np.array([[L_down_point],      #求最大矩形
                                  [L_up_point],
                                  [R_down_point],
                                  [R_up_point]])        #数组储存    y,x
                max_rec = points.max(axis=0)    #y,x
                max_rec = np.reshape(max_rec, 2)
                min_rec = points.min(axis=0)
                min_rec = np.reshape(min_rec, 2)
                #print(max_rec)
                #print(min_rec)
                
                for i in range(int(max_rec[0])- int(min_rec[0])): #y              计算最大矩形内的像素比例是否大于阈值
                    for j in range(int(max_rec[1]) - int(min_rec[1])):  #x
                        count += 1
                        if img[min_rec[0] + i][min_rec[1] + j] == 0:# 黑
                            count_Black += 1
                if count_Black/count >= 0.85:     #!!!阈值可调   
                    cross_flag =1     #全十字标志
                    repairLine(L_down_point[0], L_down_point[1], L_up_point[0], L_up_point[1], LEFT_DIR)   #左边补线
                    repairLine(R_down_point[0], R_down_point[1], R_up_point[0], R_up_point[1], RIGHT_DIR)  #右边补线

                    #重新求补线部分的中线     
                    for m_i in range(int(max_rec[0]),int(min_rec[0]),-1):
                        mid_line[m_i] = (right_boundary[m_i] + left_boundary[m_i])//2
                        #print(mid_line[m_i]) 
                       
    #上半十字识别    
    if cross_flag != 1 and L_up_flag and R_up_flag :
        if len(right_loss) > 1 and len(left_loss) > 1 :
            if left_boundary[ROW-1] == 0 and right_boundary[ROW-1] == COL-1:   #第一丢线行为最底行
                #!!!左右最终丢线行大于一定值，参数可调
                if  left_finallosey >= valid_row and  right_finallosey >= valid_row and left_finallosey>=20 and right_finallosey>=40:
                    #两个上拐点的纵坐标差值小于一定值，！！！参数可调，且左拐点的横坐标小于右拐点横坐标
                    if abs(L_up_point[0] - R_up_point[0]) < 60 and L_up_point[1] <R_up_point[1]:    #左右上拐点之间的行间距可调
                        
                        count_Black = count = 0
                        points = np.array([[L_up_point],
                                          [R_up_point]])
                        min_rec = points.min(axis=0)     #求最小纵、横坐标
                        min_rec = np.reshape(min_rec, 2)
                        for i in range((ROW-1) - int(min_rec[0])):          #可改变高度把矩形尽量调小
                            for j in range(R_up_point[1]-L_up_point[1]):
                                count+=1
                                if img[min_rec[0] + i][min_rec[1] + j] == 0:
                                    count_Black += 1
                        #print(count_Black/count)
                        if count_Black/count >= 0.85:  #!!!阈值可调  
                            cross_flag =2 #上十字标志
                            #上半十字补线
                            #左拐点往下补线
                            for i in range(L_up_point[0] +1,ROW,1 ):
                                left_boundary[i] = L_up_point[1]
                         
                            #右拐点往下补线
                            for i in range(R_up_point[0] +1 ,ROW,1):
                                right_boundary[i] = R_up_point[1]
                                            
                            #重新求补线部分的中线
                            for i in range(ROW-1,min_rec[0],-1):
                                mid_line[i] = (right_boundary[i] + left_boundary[i])//2


    #下（一个拐点）
    if (L_down_flag+L_up_flag+R_down_flag+R_up_flag)==1:    #只找到一个拐点时   有些地方不够斜
        if L_down_flag ==1:       #只找到左边的拐点（可能会和直角转弯识别产生逻辑错误）
            i = int(L_down_point[0])
            count =0
            for j in range(i,i+4,1):
                if j>=valid_row and j>20:
                    if left_boundary[j] - left_boundary[j+1]<3:
                        count+=1
            if count >=4:#!!!可调连着下面的一起调
                k = abs((left_boundary[i] - left_boundary[i+4])/(5))   #参数可调
                for i_v in range(i,20,-1):         #往上补线到40行
                    mid_line[i+1] = mid_line[i] + int(k*(i-i_v))
                cross_flag =4


    #上（一个拐点）   
    if (L_down_flag+L_up_flag+R_down_flag+R_up_flag)==1:    #只找到一个拐点时   有些地方不够斜
        if L_up_flag ==1:
            i = int(L_up_point[0])    #行
            i_invar =i
            count =0          
            for j in range(5):
                if i-1-j>=valid_row and i-1-j>20:
                    if left_boundary[i-1-j]-left_boundary[i-j]<3:
                        count+=1
            if count>=4:
                k = abs((left_boundary[i-4]-left_boundary[i])/(5*5))      #!!!参数可调，特别是*5
                #print(k)
                for i in range(i,ROW-1,1):
                    mid_line[i+1] = mid_line[i_invar] - int(k*(i-i_invar))                  
                cross_flag =3
                    
        if R_up_flag ==1:
            i = int(R_up_point[0])
            i_invar =i
            count =0
            for j in range(5):
                if i-1-j>=valid_row and i-1-j>20:
                    if right_boundary[i-j]-right_boundary[i-1-j]<3:
                        count+=1         
            if count>=4:
                k = abs((right_boundary[i]-right_boundary[i-4])/(5*5))      #!!!参数可调，特别是*5
                for i in range(i,ROW-1,1):
                    mid_line[i+1] = mid_line[i_invar] + int(k*(i-i_invar))
                cross_flag =3 
        
def getMidLine(mid_line = mid_line, left_boundary = left_boundary,
                right_boundary = right_boundary):
    '''
    description: 得到中线
    param {*}
    return {*}
    '''    
    for ii in range(19,ROW):
        mid_line[ii] = (left_boundary[ii] +right_boundary[ii])//2
    

def turnReg(road_wide=70,mid_line = mid_line,left_boundary = left_boundary,
            right_boundary = right_boundary,right_loss =right_loss,
            left_loss = left_loss):    
    '''
    description: #识别弯道，优先级低于十字(只有一边线的时候)    
    param {*}
    TODO:赛道宽度一定要调
    '''    
    global cross_flag,turn_flag
    if cross_flag !=0 :#未考虑左、右入十字
        turn_flag =0
    elif cross_flag == 0:
        #右弯道 
        if len(left_loss)<=16 and len(right_loss) >=21 :       #只有一边丢线,参数（5）可调
            if right_loss[-1]<=50:
                count_L  = 0
                for i in range(ROW-10,ROW-15,-1):         #从底向上遍历40行，左边界横坐标递增超过阈值——！！！阈值要调
                    if left_boundary[i] <= left_boundary[i-1]:
                        count_L +=1
                if count_L >=8 :   #参数可调
                    turn_flag = 1
                    #平移补线法对中线进行处理        右转补线
                    for i in range(ROW-1,0,-1):
                        if left_boundary[i]< COL and mid_line[i]< COL and i>=valid_row:  # and i>= valid_row:
                            mid_line[i] = left_boundary[i] + road_wide
                        else:
                            break
                        
                        
                # 三点求圆法（开销会有点大）
                # y1 = 15 
                # y2 = 55
                # y3 = 95
                # x1 = left_boundary[y1]
                # x2 = left_boundary[y2]
                # x3 = left_boundary[y3]
                # y2_y3 = y2-y3 
                # x2_x3 = x2-x3
                # x2y3 = x2*y3
                # x3y2 = x3*y2           
                # A = x1 * y2_y3 - y1 * x2_x3 + x2y3 - x3y2
                # if A==0:
                #     print("三点共线，无法求圆")
                # else:                                            
                #     x1x1 = x1*x1
                #     y1y1 = y1*y1
                #     x2x2 = x2*x2
                #     y2y2 = y2*y2
                #     x3x3 = x3*x3
                #     y3y3 = y3*y3
                #     x1x1py1y1 = x1x1 + y1y1
                #     x2x2py2y2 = x2x2 + y2y2
                #     x3x3py3y3 = x3x3 + y3y3
                #     B = x1x1py1y1 * (-y2_y3) + x2x2py2y2 * (y1-y3) + x3x3py3y3 * (y2-y1)
                #     C = x1x1py1y1 * x2_x3 + x2x2py2y2 * (x3 - x1) + x3x3py3y3 * (x1-x2)
                #     D = x1x1py1y1 * (x3y2 - x2y3) + x2x2py2y2 * (x1*y3 - x3*y1) + x3x3py3y3 * (x2*y1-x1*y2)
                #     x=-B/(2*A)
                #     y=-C/(2*A)
                #     rr=((B*B+C*C-4*A*D)/(4*A*A))
                #     for i in range(ROW-1,0,-1):
                #         if left_boundary[i]< COL and mid_line[i]< COL and i>=0 : 
                #             mid_line[i] = math.sqrt(rr-(i-y)*(i-y))+x
                #         else:
                #             break
                            

        
        #左弯道 
        if len(right_loss)<=8 and len(left_loss) >=11 :       #只有一边丢线,参数（5）可调
            if left_loss[-1]<=25:
                count_R  = 0
                for i in range(ROW-10,ROW-15,-1):        #从底向上遍历40行，左边界横坐标递增超过阈值——！！！阈值要调
                    if right_boundary[i] >= right_boundary[i-1]:
                        count_R +=1
                if count_R >=8 :   #参数可调
                    turn_flag = 2
                    #平移补线法对中线进行处理        右转补线
                    for i in range(ROW-1,0,-1):
                        if right_boundary[i] >=0 and mid_line[i] >= 0 and i>=valid_row:
                            mid_line[i] = right_boundary[i] - road_wide
                        else:
                            break
                # 三点求圆法（开销会有点大）
                # y1 = 15 
                # y2 = 55
                # y3 = 95
                # x1 = left_boundary[y1]
                # x2 = left_boundary[y2]
                # x3 = left_boundary[y3]
                # y2_y3 = y2-y3 
                # x2_x3 = x2-x3
                # x2y3 = x2*y3
                # x3y2 = x3*y2           
                # A = x1 * y2_y3 - y1 * x2_x3 + x2y3 - x3y2
                # if A==0:
                #     print("三点共线，无法求圆")
                # else:                                            
                #     x1x1 = x1*x1
                #     y1y1 = y1*y1
                #     x2x2 = x2*x2
                #     y2y2 = y2*y2
                #     x3x3 = x3*x3
                #     y3y3 = y3*y3
                #     x1x1py1y1 = x1x1 + y1y1
                #     x2x2py2y2 = x2x2 + y2y2
                #     x3x3py3y3 = x3x3 + y3y3
                #     B = x1x1py1y1 * (-y2_y3) + x2x2py2y2 * (y1-y3) + x3x3py3y3 * (y2-y1)
                #     C = x1x1py1y1 * x2_x3 + x2x2py2y2 * (x3 - x1) + x3x3py3y3 * (x1-x2)
                #     D = x1x1py1y1 * (x3y2 - x2y3) + x2x2py2y2 * (x1*y3 - x3*y1) + x3x3py3y3 * (x2*y1-x1*y2)
                #     x=-B/(2*A)
                #     y=-C/(2*A)
                #     rr=((B*B+C*C-4*A*D)/(4*A*A))
                #     for i in range(ROW-1,0,-1):
                #         if right_boundary[i]>=0 and mid_line[i]>=COL and i>=0 : 
                #             mid_line[i] = math.sqrt(rr-(i-y)*(i-y))+x
                #         else:
                #             break
    










 

class CRUISE:
    
    def __init__(self) -> None:
        self.midError = []
        pass
        



    def driveAuto(self):
        '''
        description: 巡航的主程序
        param {*}
        return {*}
        '''        
        global oriImg,img,cross_flag,turn_flag,left_finallosey,right_finallosey
        while True:
            ret,oriImg = cap.read()
            if ret:
                start = time.time()
                imgProcess() # 1ms左右
                
                Get_LR_Change_Points(img,y_min=20,y_max=60,
                                    right_loss=right_loss,right_boundary=right_boundary,
                                    left_loss=left_loss,left_boundary=left_boundary,mid_line=mid_line,
                                    left_valid_row=left_valid_row, right_valid_row=right_valid_row)
                
                getLastLoss()
                Find_valid_row(left_valid_row , right_valid_row)
                # print(valid_row)
                crossReg()
                #getMidLine(mid_line,left_boundary,right_boundary)
                turnReg()
                print(time.time()-start)
                mid_error = MidErrorCal(mid_line=mid_line)
                print(mid_error)
                self.midError.append(mid_error)
                drawImg(left_boundary,right_boundary,mid_line,img)
                # print(cross_flag)
                # print(turn_flag)
                right_loss.clear()
                left_loss.clear()

                right_valid_row.clear()
                left_valid_row.clear()
                # 标志位重置
                cross_flag = 0     
                turn_flag =0       
                left_finallosey =0
                right_finallosey =0
                # cv2.imshow('1',img)
                # cv2.waitKey(0)
            else:
                with open('60.txt','w') as f:
                    f.writelines(str(self.midError))
                    f.close()
                break

    
cruiseObject = CRUISE() 
cruiseObject.driveAuto()
