import os
import sys
sys.path.append("/home/root/workspace/17BaiduAI_V1") 
import cv2
import time

from Modules.Camera.CameraMulti import frontCamObj
from Modules.Vision.Preprocess import sideParkObj
from Modules.Serial.Serial import serialObj
from Modules.Config import logging


if __name__ == '__main__':
    frontCamObj.start()
    time.sleep(0.1)
    while 1:
        logging.info("hello")
        img = frontCamObj.read()
        logging.info("read")
        
        if img is not None:
            ret = sideParkObj.detect(img)
            logging.info('hello:{}'.format(ret))
            if 160-ret > 10:
                serialObj.writeData([0xAA,0xDE,100,-30+100,100,0xFF])
            elif 160-ret <-10:
                serialObj.writeData([0xAA,0xDE,100,30+100,100,0xFF])
            else:
                serialObj.writeData([0xAA,0xDE,100,0+100,100,0xFF])
        else:
            logging.info("no pic")
        # time.sleep(0.1)

