'''
Author: your name
Date: 2022-05-03 00:47:59
LastEditTime: 2022-05-03 14:01:42
LastEditors: Please set LastEditors
Description: 多进程测试两个摄像头同时获得图像，成功
FilePath: \17th_BAIDUAI_V1\Test\test2.py
'''
import os
import sys
sys.path.append("/home/root/workspace/17BaiduAI_V2") 
from Modules.Camera.CameraMulti import frontCamObj,rightCamObj
from Modules.Lock.Lock import frontCamStop_SHARE
import cv2
import time
import numpy as np
import time


if __name__ == '__main__':
    frontCamObj.start()
    i = 0
    while 1:

        img = frontCamObj.read()
        if img is not None:
            cv2.imwrite("TmpImg/sign/"+str(i)+".jpg",img)
            print(i)
            time.sleep(0.03)
            i +=1 

        
        # i = i + 1
        # if(i == 2000):
        #     frontCamObj.pause()
        #     print("pause")
        #     print("value",frontCamStop_SHARE.value)
        #     break