import os
import sys
sys.path.append("D:\\大学\\课外项目、比赛、学习\\2022车队\\2022百度创意\\edgeboard相关\\17th_BAIDUAI_V2") 
import cv2
import time
from Modules.Camera.CameraMulti import leftCamObj
from Modules.Vision.ModelTest import taskDetectObj
from Modules.Config import logging
from Modules.Cruise.CruiseCpp import cruiseObj 
"""
note：
适用于电脑运行，查看模型的效果
目前尚未移植完成，因为window的多进程跟linux有区别，目前运行不了

"""
leftCamObj.start()
# rightCamObj.start()
time.sleep(0.1)
# frontCamObj.start()
# time.sleep(0.1)
# cruiseObj.start()
while True:
    try:
        if taskDetectObj.testTask(2) :
            # and 150 < taskDetectObj.x < 160
            logging.info("task x :{},task y:{},task score:{}".format(taskDetectObj.x,taskDetectObj.y,taskDetectObj.score))
            # 发送停车指令给下位机
            # serialObj.writeData([0xAA, 0xDE, 1, 0, 0, 0xFF])
            # logging.info("pause pause")
            # cruiseObj.pause() # 暂停巡航
            # frontCamObj.pause() # 暂时关闭前摄像头
            # break
    except Exception as e:
        print(e)
        continue