#!/usr/bin/python
# coding=UTF-8
import os
import sys
sys.path.append("/home/root/workspace/17BaiduAI_V4") 
import cv2
import time
from Modules.Camera.CameraMulti import rightCamObj
from Modules.Vision.ModelTest import *
from Modules.Config import logging
from Modules.Cruise.CruiseCpp import cruiseObj 
from Modules.Vision.Preprocess import purchasePreObj

rightCamObj.start()
time.sleep(0.1)
while True:
    try:
        img = rightCamObj.readForDetect()
        detectX,detectY = purchasePreObj.detect(img)
    except Exception as e:
        print(e)
        continue