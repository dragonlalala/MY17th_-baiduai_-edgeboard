import os
import sys
sys.path.append("/home/root/workspace/17BaiduAI_V3") 
import cv2
import time

from Modules.Camera.CameraMulti import rightCamObj,frontCamObj
from Modules.Cruise.CruiseCpp import cruiseObj

frontCamObj.start()
time.sleep(0.1)
cruiseObj.start()
while 1:
    img = frontCamObj.readForCruise()
    if img is not None:
        cv2.imwrite("cruise.jpg",img)
        time.sleep(1)
    pass

