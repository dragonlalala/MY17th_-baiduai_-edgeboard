/*
 * @Author: your name
 * @Date: 2022-04-19 16:16:12
 * @LastEditTime: 2022-04-24 20:18:03
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \cruise_cpp\cruise.cpp
 */

#include <iostream>
#include <vector>
#include <string>
#include <fstream>
#include <typeinfo>
#include <ctime>
#include <algorithm>
#include <string.h>

using namespace std ;


extern "C"{
// 宏定义区
#define COL 160
#define ROW 120
#define MIDDLE int(COL/2)
#define BLACK 0
#define WHITE 255
#define MAT_FROM_TXT
#define LEFT_DIR 0
#define RIGHT_DIR 1
#define Y_MIN 39
#define Y_MAX  ROW-1
#define HALF_ROAD_WIDTH 70
// typedef 区
typedef int16_t int16;
typedef unsigned char uchar;
// 主要的数据结构，有左边和右边
struct Direction{
    int16 boundary[ROW];
    vector<int16>valid_row;
    vector<int16>loss;
    int16 finallosey;
    vector<int16>possible_up_cross_point;
    vector<int16>possible_down_cross_point;
};
// 描述一个点的结构体
struct Point{
    int16 x;
    int16 y;
};

//全局变量区
uchar img[ROW][COL] ={0};
int16 midLine[ROW]={0};
int16 validRow = 0;
bool cross_flag = false;
bool turn_flag = false;

// 绝对值函数
int16 myabs(int16 a, int16 b){
    int16 c = a-b;
    if(c>=0)return c;
    else return -c;
}


void getMat(uchar* matrix){
    for(int i = 0;  i < ROW; i++){
        for(int j = 0; j <COL; j++){
            img[i][j] = matrix[i*COL + j];
        }
    }
}

// 从文本文件中创建矩阵
void creatMat(uchar img[ROW][COL], string fileName){
    ifstream infile;//定义读取文件流，相对于程序来说是in
	infile.open(fileName);//打开文件
    for (int i = 0; i < ROW; i++)//定义行循环
	{
		for (int j = 0; j < COL; j++)//定义列循环
		{
			infile >> img[i][j];//读取一个值（空格、制表符、换行隔开）就写入到矩阵中，行列不断循环进行
		}
	}
	infile.close();//读取完成之后关闭文件
    
}

// 从中间往左边找到的传统寻找跳变点方法
int16 leftFindChange(Direction* lleft, int16 tmp_y,int16 start_x = MIDDLE){
    bool left_have_found = 0;
    for(int x = start_x; x > 0; x-=3){
        if(img[tmp_y][x] == 255){
            if(img[tmp_y][x+1] == 0){
                lleft->boundary[tmp_y] = x;
                left_have_found = 1;
                break;
            }
            else if(img[tmp_y][x+2] == 0){
                lleft->boundary[tmp_y] = x+1;
                left_have_found = 1;
                break;
            }
            else if(img[tmp_y][x+3] == 0){
                lleft->boundary[tmp_y] = x+2;
                left_have_found = 1;
                break;
            }
            else{
                if(tmp_y < 100)
                    lleft->valid_row.push_back(tmp_y);  //防止地标出现在100-119行间，导致进入有效行数组
            }
        }
    }
    if(!left_have_found){
        lleft->boundary[tmp_y] = 0;
        lleft->loss.push_back(tmp_y);
    }
    return lleft->boundary[tmp_y];
}

// 从中间往右边找到的传统寻找跳变点方法
int16 rightFindChange(Direction* rright, int16 tmp_y,int16 start_x = MIDDLE){
    bool right_have_found = 0;
    for(int x = start_x; x < COL; x+=3){
        if(img[tmp_y][x] == 255){
            if(img[tmp_y][x-1] == 0){
                rright->boundary[tmp_y] = x;
                right_have_found = 1;
                break;
            }
            else if(img[tmp_y][x-2] == 0){
                rright->boundary[tmp_y] = x-1;
                right_have_found = 1;
                break;
            }
            else if(img[tmp_y][x-3] == 0){
                rright->boundary[tmp_y] = x-2;
                right_have_found = 1;
                break;
            }
            else{
                if(tmp_y < 100)
                    rright->valid_row.push_back(tmp_y);  //防止地标出现在100-119行间，导致进入有效行数组
            }
        }
    }
    if(!right_have_found){
        rright->boundary[tmp_y] = COL -1;
        rright->loss.push_back(tmp_y);
    }
    return rright->boundary[tmp_y];
}

/**
 * @description: 八领域方法寻找左边边界跳变点
 * @param {Direction*} lleft left结构体
 * @param {int16} tmp_y 当前的y
 * @param {int16} last_x 上一个左边界
 * @param {int16} last_mid 上一个中点
 * @return {*}
 */
int16 filedLeftFindChange(Direction* lleft,int16 tmp_y,int16 last_x,int16 last_mid=MIDDLE){
    bool left_have_found = 0;
    if(last_x != 0){
        if(img[tmp_y][last_x] == 255){
            for (int x = last_x + 1; x < COL; x++){
               if((img[tmp_y][x] == 0) &&  (img[tmp_y][x-1] == 255)){
                   lleft->boundary[tmp_y] = x-1;
                   left_have_found = 1;
                   break;
               }
            }
        }
        else{
            for (int x = last_x - 1; x > -1; x--){
               if((img[tmp_y][x] == 255) &&  (img[tmp_y][x+1] == 0)){
                   lleft->boundary[tmp_y] = x;
                   left_have_found = 1;
                   break;
               }
            }
        }
    }
    else{
        leftFindChange(lleft,tmp_y,last_mid);
        left_have_found = 1;
    }
    if(!left_have_found){
        lleft->boundary[tmp_y] = 0;
        lleft->loss.push_back(tmp_y);
    }
    return lleft->boundary[tmp_y];
}

/**
 * @description: 八领域寻找右边边界跳变点
 * @param {Direction*} rright 右边的数据结构
 * @param {int16} tmp_y 当前的Y
 * @param {int16} last_x 上一个右边边界点
 * @param {int16} last_mid 上一个中点
 * @return {*}
 */
int16  filedRightFindChange(Direction* rright,int16 tmp_y,int16 last_x,int16 last_mid=MIDDLE){
    bool right_have_found = 0;
    if (last_x != COL-1){
        if(img[tmp_y][last_x] == 255){
            for(int x = last_x-1; x >-1;x--){
                if((img[tmp_y][x] == 0)&&(img[tmp_y][x+1] == 255)){
                    rright->boundary[tmp_y] = x+1;
                    right_have_found = 1;
                    break;
                }
            }
            if(!right_have_found){
                rright->valid_row.push_back(tmp_y);
                rright->boundary[tmp_y] = COL -1;
                right_have_found = 1;
            }
        }
        else{
            for(int x = last_x+1; x < COL; x++){
                if((img[tmp_y][x] == 255)&& img[tmp_y][x-1] == 0){
                    rright->boundary[tmp_y] = x;
                    right_have_found =1;
                    break;
                }
            }
        }
    } 
    else{
        rightFindChange(rright,tmp_y,last_mid);
        right_have_found = 1;
    }
    if(!right_have_found){
        rright->boundary[tmp_y] = COL-1;
        rright->loss.push_back(tmp_y);
    }
    return rright->boundary[tmp_y];

}


/**
 * @description: 八领域生成左右边界的
 * @param {Direction*} pleft
 * @param {Direction*} pright
 * @param {int16} y_min
 * @param {int16} y_max
 * @return {*}
 */
void filedGetLRChangePoints(Direction* pleft,Direction* pright, int16 y_min=0,int16 y_max=60){
    int16 curr_right = 0;
    int16 curr_left = 0;
    int16 curr_mid = 0;
    curr_right =rightFindChange(pright,ROW-1,MIDDLE);
    curr_left = leftFindChange(pleft, ROW-1, MIDDLE);
    curr_mid = (int16)(curr_right+curr_left)/2;
    midLine[ROW-1] = curr_mid;
    for(int y = ROW-2;y > y_min-1; y--){
        curr_right = filedRightFindChange(pright,y,curr_right,curr_mid);
        curr_left = filedLeftFindChange(pleft,y,curr_left,curr_mid);
        midLine[y] = (int16)(curr_right+curr_left)/2;
        curr_mid = midLine[y];
    }

}

/**
 * @description: 寻找最远有效行
 * @param {Direction*} pleft
 * @param {Direction*} pright
 * @return {*}
 */
void findValidRow(Direction* pleft,Direction* pright){
    int16 L_len = pleft->valid_row.size();
    int16 R_len = pright->valid_row.size();
    if((L_len==0) && (R_len>0)) 
        validRow = pright->valid_row[0];
    if (L_len >0 && R_len==0)
        validRow = pleft->valid_row[0];
    if(L_len >0 && R_len>0){
         if (pleft->valid_row[0]<pright->valid_row[0])
            validRow = pright->valid_row[0];
        else
            validRow = pleft->valid_row[0];
    }
}

/**
 * @description: 根据两点修复一条直线
 * @param {Direction*} pleft
 * @param {Direction*} pright
 * @param {int16} down_x
 * @param {int16} down_y
 * @param {int16} up_x
 * @param {int16} up_y
 * @param {bool} dir
 * @return {*}
 */
void repairLine(Direction* pleft, Direction* pright, int16 down_x, int16 down_y, int16 up_x, int16 up_y,bool dir){
    if (dir ==LEFT_DIR){
       float k = (float)(up_x -down_x)/(down_y - up_y);
       for(int i = down_y -1; i > up_y; i--){
           pleft->boundary[i] = down_x +(int)(k*(down_y-i));
           img[i][pleft->boundary[i]] =100;
       }
    }
    else{
        float k = (float)( down_x - up_x)/(down_y - up_y);
        for(int i = down_y -1; i > up_y; i--){
           pright->boundary[i] = down_x - (int)(k*(down_y-i));
           img[i][pright->boundary[i]] =100;
        }
    } 
}


void reCalMidLine(Direction* pleft,Direction* pright){
    for(int i = 0; i<ROW-1; i++){
        midLine[i] = (int)((pleft->boundary[i]+pright->boundary[i])/2);
    }
}

/**
 * @description: 八领域寻找是否是跳变点
 * @param {int} i
 * @param {int} j
 * @return {*}
 */
int16 findNineSquares(int i, int j){
    int16 count = 0;
    for (int k = 0; k < 3; k++)
    {
        for(int l = 0; l < 3; l++){
            if (img[i-1+k][j-1+l]==255){
                count++;
            }
        }
    }
    return count;
    
}


/**
 * @description: 获取可能的十字拐点
 * @param {Direction*} pleft
 * @param {Direction*} pright
 * @param {int16} y_min
 * @return {*}
 */
void getPossibleCrossPoint(Direction* pleft,Direction* pright,int16 y_min = 40){
    for(int i = ROW -1; i > y_min ;i--){
        if((pleft->boundary[i] == 0)&&(pleft->boundary[i-1] !=0)){
            pleft->possible_up_cross_point.push_back(i-1);
        }
        if((pleft->boundary[i] != 0)&&(pleft->boundary[i-1] ==0)){
            pleft->possible_down_cross_point.push_back(i);
        }
        if((pright->boundary[i] == COL-1)&&(pright->boundary[i-1] != COL-1)){
            pright->possible_up_cross_point.push_back(i-1);
        }
        if((pright->boundary[i] != COL-1)&&(pright->boundary[i-1] ==COL-1)){
            pright->possible_down_cross_point.push_back(i);
        }
    }
}


/**
 * @description: 十字识别
 * @param {int} y_min
 * @param {Direction*} pleft
 * @param {Direction*} pright
 * @return {*}
 */
void crossReg(int y_min,Direction* pleft, Direction* pright){
    bool R_down_flag =0;   
    bool R_up_flag = 0;
    bool L_down_flag = 0;
    bool L_up_flag = 0;
    getPossibleCrossPoint(pleft,pright,0);
    // 先找右边
    // 右下拐点
    int r_down_count = pright->possible_down_cross_point.size();
    for(int item = r_down_count-1 ;item >= 0;item --){
        int16 tmp_y = pright->possible_down_cross_point[item];
        if((tmp_y>=5) && (tmp_y<= ROW -5)&&(tmp_y > validRow)){
            for (int i = tmp_y; i <= tmp_y + 4; i++){
                if(pright->boundary[i]<=pright->boundary[i+1] 
                && pright->boundary[i+1] <= pright->boundary[i+2]
                && myabs(pright->boundary[i],pright->boundary[i+2])<5
                && pright->boundary[i]<COL-10){
                    pright->possible_down_cross_point[item]=i;
                    break;
                }
                if(i == tmp_y+4){
                    //删除掉不符合的
                    pright->possible_down_cross_point.erase(pright->possible_down_cross_point.begin()+item);
                }
            }
        }
        else pright->possible_down_cross_point.erase(pright->possible_down_cross_point.begin()+item);
        //tmp_y >COL-5的不处理  
    }
    //右上拐点
    int16 r_up_count = pright->possible_up_cross_point.size();
    for(int item = r_up_count-1 ;item >= 0;item --){
        int16 tmp_y = pright->possible_up_cross_point[item];
        if((tmp_y>=5) && (tmp_y<= ROW -5)&&(tmp_y > validRow)){
            for (int i = tmp_y; i >= tmp_y - 4; i--){
                if(pright->boundary[i]>=pright->boundary[i-1] 
                && pright->boundary[i-1] >= pright->boundary[i-2]
                && myabs(pright->boundary[i],pright->boundary[i-2])<5
                && pright->boundary[i] < COL-10){
                    pright->possible_up_cross_point[item]=i;
                    break;
                }
                if(i == tmp_y-4){
                    //删除掉不符合的
                    pright->possible_up_cross_point.erase(pright->possible_up_cross_point.begin()+item);
                }
            }
        }
        else pright->possible_up_cross_point.erase(pright->possible_up_cross_point.begin()+item);
        //tmp_y >COL-5的不处理  
    }
    // 左下拐点
    int16 l_dowm_count  = pleft->possible_down_cross_point.size();
    for(int item = l_dowm_count-1 ;item >= 0;item --){
        int16 tmp_y = pleft->possible_down_cross_point[item];
        if((tmp_y>=5) && (tmp_y<= ROW -5)&&(tmp_y > validRow)){
            for (int i = tmp_y; i <= tmp_y + 4; i++){
                if(pleft->boundary[i]>=pleft->boundary[i+1] 
                && pleft->boundary[i+1] >= pleft->boundary[i+2]
                && myabs(pleft->boundary[i],pleft->boundary[i+2])<5
                && pleft->boundary[i]>10){
                    pleft->possible_down_cross_point[item]=i;
                    break;
                }
                if(i == tmp_y+4){
                    //删除掉不符合的
                    pleft->possible_down_cross_point.erase(pleft->possible_down_cross_point.begin()+item);
                }
            }
        }
        else pleft->possible_down_cross_point.erase(pleft->possible_down_cross_point.begin()+item);
        //tmp_y >COL-5的不处理  
    }
    //左上拐点
    int16 l_up_count  = pleft->possible_up_cross_point.size();
    for(int item = l_up_count-1 ;item >= 0;item --){
        int16 tmp_y = pleft->possible_up_cross_point[item];
        if((tmp_y>=5) && (tmp_y<= ROW -5)&&(tmp_y > validRow)){
            for (int i = tmp_y; i >= tmp_y - 4; i--){
                if((pleft->boundary[i]<=pleft->boundary[i-1])
                && (pleft->boundary[i-1] <= pleft->boundary[i-2])
                &&  (myabs(pleft->boundary[i],pleft->boundary[i-2])<5)
                && (pleft->boundary[i]>10)
                && (i>10)){
                    pleft->possible_up_cross_point[item]=i;
                    break;
                }
                if(i == tmp_y-4){
                    //删除掉不符合的
                    pleft->possible_up_cross_point.erase(pleft->possible_up_cross_point.begin()+item);
                }
            }
        }
        else pleft->possible_up_cross_point.erase(pleft->possible_up_cross_point.begin()+item);
        //tmp_y >COL-5的不处理  
    }
    // 为了刚好地显示
    for (int i = 0; i < pleft->possible_down_cross_point.size(); i++)
    {
        int16 tmp_y = pleft->possible_down_cross_point[i];
        img[tmp_y][pleft->boundary[tmp_y]] = 100;
    }
    for (int i = 0; i < pleft->possible_up_cross_point.size(); i++)
    {
        int16 tmp_y = pleft->possible_up_cross_point[i];
        img[tmp_y][pleft->boundary[tmp_y]] = 100;
    }
    for (int i = 0; i < pright->possible_down_cross_point.size(); i++)
    {
        int16 tmp_y = pright->possible_down_cross_point[i];
        img[tmp_y][pright->boundary[tmp_y]] = 100;
    }
    for (int i = 0; i < pright->possible_up_cross_point.size(); i++)
    {
        int16 tmp_y = pright->possible_up_cross_point[i];
        img[tmp_y][pright->boundary[tmp_y]] = 100;
    }

}


/**
 * @description: 十字类型的识别，并修复
 * @param {Direction*} pleft
 * @param {Direction*} pright
 * @return {*}
 */
void regCrossType(Direction* pleft,Direction* pright){
    int16 l_up_count = pleft->possible_up_cross_point.size();
    int16 l_down_count = pleft->possible_down_cross_point.size();
    int16 r_up_count = pright->possible_up_cross_point.size();
    int16 r_down_count = pright->possible_down_cross_point.size();
    Point L_UP,L_DOWN,R_UP,R_DOWN;
    if(l_up_count!=0){
        L_UP.y = pleft->possible_up_cross_point.back();
        L_UP.x = pleft->boundary[L_UP.y];
    }
    if(l_down_count!=0){
        L_DOWN.y = pleft->possible_down_cross_point[0];
        L_DOWN.x = pleft->boundary[L_DOWN.y];
    }
    if(r_up_count!=0){
        R_UP.y = pright->possible_up_cross_point.back();
        R_UP.x = pright->boundary[R_UP.y];
    }
    if(r_down_count!=0){
        R_DOWN.y = pright->possible_down_cross_point[0];
        R_DOWN.x = pright->boundary[R_DOWN.y];
    }
    // 修复左边
    if(l_down_count && l_up_count){
        if(L_DOWN.y > L_UP.y){
            repairLine(pleft,pright,L_DOWN.x,L_DOWN.y,L_UP.x,L_UP.y,LEFT_DIR);
            cross_flag = true;
        }
        else{
            repairLine(pleft,pright,L_DOWN.x,L_DOWN.y,L_DOWN.x+10,0-1,LEFT_DIR);
            repairLine(pleft,pright,0,ROW-1,L_UP.x,L_UP.y,LEFT_DIR);
            cross_flag = true;
        }
        
    }
    else if(l_down_count && (l_up_count==0)){
        repairLine(pleft,pright,L_DOWN.x,L_DOWN.y,L_DOWN.x +10,0-1,LEFT_DIR);
        cross_flag = true;
    }
    else if(l_up_count && (l_down_count==0)){
        repairLine(pleft,pright,0,ROW-1,L_UP.x,L_UP.y,LEFT_DIR);
        cross_flag = true;
    }
    else{
       cross_flag = false;
    }
    // 修复右边
    if(r_down_count && r_up_count ){
        if (R_DOWN.y > R_UP.y){
            repairLine(pleft,pright,R_DOWN.x,R_DOWN.y,R_UP.x,R_UP.y,RIGHT_DIR);
            cross_flag = true;
        }
        else{
            repairLine(pleft,pright,R_DOWN.x,R_DOWN.y,R_DOWN.x-10,0-1,RIGHT_DIR);
            repairLine(pleft,pright,COL-1,ROW-1,R_UP.x,R_UP.y,RIGHT_DIR);
            cross_flag = true;
        }   
    }
    else if(r_down_count && (r_up_count==0)){
        repairLine(pleft,pright,R_DOWN.x,R_DOWN.y,R_DOWN.x -10,0-1,RIGHT_DIR);
        cross_flag = true;
    }
    else if(r_up_count && (r_down_count==0) && (R_UP.y >25)){
        repairLine(pleft,pright,COL-1,ROW-1,R_UP.x,R_UP.y,RIGHT_DIR);
        cross_flag = true;
    }
    else{
        cross_flag = false;
    }

    //特殊情况
    if(r_down_count && l_up_count==0 && l_down_count==0){
        cross_flag =false;
    }
    if(cross_flag){
        reCalMidLine(pleft,pright);
    }
}


void turnReg(Direction* pleft,Direction* pright){
    if(cross_flag)turn_flag = false;
    else{
        int16 left_loss_count = pleft->loss.size();
        int16 right_loss_count = pright->loss.size();
        // 右弯道
        if(left_loss_count < 50 && right_loss_count >80 && *max_element(&pleft->boundary[0],&pleft->boundary[ROW-1])>40){
            // 平移补线法
            for (int i = ROW-1; i > -1; i--){
                midLine[i] = pleft->boundary[i] + HALF_ROAD_WIDTH;
            }  
            turn_flag  = true;
        }
        else if(right_loss_count < 50 && left_loss_count >80 && *min_element(&pright->boundary[0],&pright->boundary[ROW-1])<120){
            // 平移补线法
            for (int i = ROW-1; i > -1; i--) {
                midLine[i] = pright->boundary[i] - HALF_ROAD_WIDTH;
            }
            turn_flag  = true;
        }
    }
}



void showBoundary(Direction* pleft,Direction* pright){
    for (int i = 0; i < ROW; i++){
        img[i][pleft->boundary[i]] = 20;
    }
    for (int i = 0; i < ROW; i++){
        img[i][pright->boundary[i]] = 20;
    }
    
    for (int i = 0; i < ROW; i++){
        if (midLine[i] <=0){
            midLine[i] = 0;
        }
        img[i][midLine[i]] = 20;
    }   
}


/**
 * @description: 显示数组到文件中
 * @param {int16} img
 * @return {*}
 */
void showMat(uchar img[][COL],Direction* pleft, Direction* pright){
    showBoundary(pleft, pright);
    ofstream outfile;
    outfile.open("output.txt");
    for (int i = 0; i < ROW; i++){
		for (int j = 0; j < COL; j++){
            if(img[i][j]==255){
                outfile<<"*";
            }
            else if(img[i][j]==100){
                outfile<<"@";
            }
            else if(img[i][j]==20){
                outfile<<"$";
            }
            else{
                outfile<<".";
            }	
		}
        outfile<<endl;
	}
    outfile.close();

}

int calMidError(){
    int sum = 0;
    if(validRow <= 60){
        for(int i = 60; i<ROW; i++){
            sum +=midLine[i];
        }
        return (int)((sum-MIDDLE*60)/60);
    }
    else{
        for(int i = (int)validRow; i<ROW; i++){
            sum +=midLine[i];
        }
        return (int)((sum-MIDDLE*(120-validRow))/(120-validRow));
    }
    
}

auto getFileNames()
{
    vector<string>filelist;
    string tmp_name;
    ifstream infilename;//定义读取文件流，相对于程序来说是in
	infilename.open("filename.txt");//打开文件
    for(int j =0;j<1429;j++){
        infilename>>tmp_name;
        filelist.push_back(tmp_name);
        // cout<<tmp_name;
    }
    return filelist;
}


// int main(){
//     clock_t startTime,endTime;
    
//     auto namelist = getFileNames();
    
//     for(string name:namelist){
//         cout<<name<<endl;
//         if(name =="imgTxt/8701.jpg.txt"){
//             cout<<endl;
//         }
        
//         creatMat(img,name);
//         startTime = clock();//计时开始
//         Direction* sruct_left = new Direction;
//         Direction* struct_right = new Direction;
//         filedGetLRChangetPoints(struct_left,struct_right,0);
//         for(int16 item:struct_right->valid_row){
//             cout<<"loss:"<<item<<endl;
//         }
//         findValidRow(struct_left,struct_right);
//         crossReg(0,struct_left,struct_right);
//         regCrossType(struct_left,struct_right);
//         turnReg(struct_left,struct_right);
        
//         showMat(img,struct_left,struct_right);
//         cout<<"turn_flag:"<<turn_flag<<endl;
//         cout<<"cross_flag:"<<cross_flag<<endl;
//         cout<<"validRow:"<<validRow<<endl;
//         delete struct_left,struct_right;
//         turn_flag = false;
//         cross_flag = false;
//         validRow = 0;
//         endTime = clock();//计时结束
//         cout<<"t:"<<(double)(endTime - startTime) /1000<<endl;
//         getchar();
//     }  
    
//     return 0 ;
// }

int cruise(uchar* matrix){
    clock_t startTime,endTime;
    startTime = clock();//计时开始
    getMat(matrix);
    Direction* struct_left = new Direction;
    Direction* struct_right = new Direction;
    filedGetLRChangePoints(struct_left,struct_right,0);
    findValidRow(struct_left,struct_right);
    crossReg(0,struct_left,struct_right);
    regCrossType(struct_left,struct_right);
    turnReg(struct_left,struct_right);
    showMat(img,struct_left,struct_right);
    // cout<<"turn_flag:"<<turn_flag<<endl;
    // cout<<"cross_flag:"<<cross_flag<<endl;
    // cout<<"validRow:"<<validRow<<endl;
    int mid_error = calMidError();
    // cout<<"error:"<<mid_error<<endl;
    delete struct_left,struct_right;
    turn_flag = false;
    cross_flag = false;
    validRow = 0;
    endTime = clock();//计时结束
    // cout<<"cpp time:"<<(double)(endTime - startTime) /1000<<endl;
    return mid_error;
}




// LINUX编译：g++ helloworld.cpp -o helloworld
// 运行：./helloworld
// 编译成动态库：g++ -o cruise.so -shared -fPIC cruise.cpp


}