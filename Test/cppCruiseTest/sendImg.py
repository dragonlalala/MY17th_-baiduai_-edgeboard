import cv2
import ctypes
from ctypes import string_at
import numpy as np
import time

lib = ctypes.cdll.LoadLibrary("./cruise.so")
uint8_type = np.ctypeslib.ndpointer(ctypes.c_ubyte)
lib.cruise.argtypes = [uint8_type]
lib.cruise.restype =ctypes.c_int
element = cv2.getStructuringElement(cv2.MORPH_RECT, (3, 3))
class Cruise:
    def __init__(self):
        self.cap = cv2.VideoCapture(0)
        self.cap.set(cv2.CAP_PROP_FRAME_WIDTH, 320)
        self.cap.set(cv2.CAP_PROP_FRAME_HEIGHT, 240)
        self.oriImg = np.zeros((120,160),dtype=np.uint8)
        self.img = np.zeros((120,160),dtype=np.uint8)
        self.lower = np.array([22, 43, 46])  # 黄色的下阈值
        self.upper = np.array([34, 255, 255])
        pass
    
    def imgProcess(self):
        '''
        description: 处理图片得到二值化图片
        param {*}
        return {*}
        '''    
        
        img_resize = cv2.resize(self.oriImg,(160,120),interpolation=cv2.INTER_NEAREST) #0.4ms
        img_hsv = cv2.cvtColor(img_resize,cv2.COLOR_BGR2HSV) # 1.1ms->0.3ms
        mask = cv2.inRange(img_hsv,self.lower,self.upper)    # 1ms ->0.3ms
        imgdilate = cv2.dilate(mask, element)  # 膨胀                     #0.4ms
        self.img = cv2.morphologyEx(imgdilate, cv2.MORPH_CLOSE, element)  # 先腐蚀后膨胀操作，原为腐蚀操作 #0.4ms
        
    def send(self):
        # self.img.astype(np.int16)
        b = lib.cruise(self.img)
        print(b)


crobject = Cruise()

while 1:
    ret, crobject.oriImg = crobject.cap.read()
    if ret:
        start = time.time()
        crobject.imgProcess()
        crobject.send()
        print("time:",time.time()-start)
        
        
        