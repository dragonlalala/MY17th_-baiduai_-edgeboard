'''
Author: your name
Date: 2022-04-18 10:34:24
LastEditTime: 2022-04-22 10:25:01
LastEditors: Please set LastEditors
Description: In User Settings Edit
FilePath: \17th_BAIDUAI_V1\Test\test.py
'''
import cv2
import numpy as np
import os
lower = np.array([12, 59, 173])  # 黄色的下阈值
upper = np.array([29, 202, 255])     
import ctypes
                   
path ='./Pictures/'
fileNamelist = os.listdir(path)

for item in fileNamelist:
    oriImg = cv2.imread(path+item)                

    img_hsv = cv2.cvtColor(oriImg,cv2.COLOR_BGR2HSV)
    # ！！！图像HSV阈值可以再调精确些
    mask = cv2.inRange(img_hsv,lower,upper)
    # TODO:实际可能需要降噪措施。
    img = cv2.resize(mask,(160,120),interpolation=cv2.INTER_NEAREST)
    element = cv2.getStructuringElement(cv2.MORPH_RECT, (3, 3))
    imgdilate = cv2.dilate(img, element)  # 膨胀
    img = cv2.morphologyEx(imgdilate, cv2.MORPH_CLOSE, element)  # 先腐蚀后膨胀操作，原为腐蚀操作
    # img = cv2.resize(img,(80,60),interpolation=cv2.INTER_NEAREST)
    cv2.imshow('1',img)
    # np.savetxt('OutputTxt/'+item+'.txt',img,fmt='%3d',newline=' ')
                  
    print(item+'\n')
    cv2.waitKey(0)