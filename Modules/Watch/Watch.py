'''
Author: your name
Date: 2022-04-28 10:26:58
LastEditTime: 2022-04-28 14:50:25
LastEditors: Please set LastEditors
Description: 
FilePath: \GITBaidu2022\Modules\Watch\Watch.py
'''

import contextlib
import os
import signal
from Modules.Config import logging
from Modules.Camera.CameraMulti import frontCamObj,rightCamObj,leftCamObj
class Watcher():
    # 解决使用 ctrl+c 不能杀死程序的问题

    def __init__(self):
        self.child = os.fork()
        if self.child == 0:
            return
        else:
            self.watch()

    def watch(self):
        try:
            os.wait()
        except KeyboardInterrupt:
            self.kill()
        
        logging.debug("############################# Watch Dog Kill All Python Process ###############################")
        os.system('pkill -9 python')
        os._exit(0)

    def kill(self):
        with contextlib.suppress(OSError):
            os.kill(self.child, signal.SIGKILL)

# watcherObj = Watcher()
