'''
Author: your name
Date: 2022-04-09 20:44:47
LastEditTime: 2022-08-15 00:29:14
LastEditors: Please set LastEditors
Description: In User Settings Edit
FilePath: \17th_BAIDUAI_V1\Modules\Lock\Lock.py
'''
"""
存放共享变量和进程锁,参考资料：https://blog.csdn.net/s_o_l_o_n/article/details/92148720
"""

import multiprocessing

lock_serial = multiprocessing.Manager().Lock()
lock_sign = multiprocessing.Manager().Lock()
lock_use_fpga = multiprocessing.Manager().Lock()

# 3个摄像头的暂停重启的共享标志位
frontCamStop_SHARE = multiprocessing.Value("i", False)
leftCamStop_SHARE =  multiprocessing.Value("i", False)
rightCamStop_SHARE =  multiprocessing.Value("i", False)

# 地标检测的暂停重启的共享标志位
signDetectStop_SHARE = multiprocessing.Value("i", False)
# 地标检测到的地标下标的共享变量
signIndex_SHARE = multiprocessing.Value("i", 0)
# 地标检测到的地标的X,Y的像素坐标共享变量
signX_SHARE =  multiprocessing.Value("i", 0) 
signY_SHARE =  multiprocessing.Value("i", 0) 
# 地标检测到的地标的height的共享变量
signHeight_SHARE = multiprocessing.Value("i", 0)
# 地标检测到的地标的width的共享变量
signWidth_SHARE = multiprocessing.Value("i", 0)
# 地标检测到的地标是否符合想要的条件共享变量
# signIsSuccess_SHARE = multiprocessing.Value("i", 0)
# 巡航的暂停重启的共享标志位
cruiseStop_SHARE = multiprocessing.Value("i", False)
# 决定巡航的偏差由cruise进程来发送还是主进程来发送，True为curise进程发送，当在做任务时，由主进程来发送
cruiseSendMidErrorFlag_SHARE = multiprocessing.Value("i", True) 
# 与主进程共享的偏差
cruiseMidError_SHARE = multiprocessing.Value("i", 0) 
# 执行地标检测还是巡航检测
cruise_or_sign_SHARE = multiprocessing.Value("i", 0) 