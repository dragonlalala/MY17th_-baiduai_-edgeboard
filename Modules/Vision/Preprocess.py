'''
Author: your name
Date: 2022-05-03 00:47:59
LastEditTime: 2022-07-20 01:00:32
LastEditors: Please set LastEditors
Description: 用于放歌友谊那里的识别蓝色以及颜色块的识别
FilePath: 
'''

import cv2
import numpy as np
from Modules import Config

class sideParkClass:
    def __init__(self):
        self.lower = np.array([100, 78, 79])  # 蓝色的下阈值
        self.upper = np.array([116, 164, 183])
        self.element = cv2.getStructuringElement(cv2.MORPH_RECT, (3, 3))
        self.contours = None
        self.count = 0
        
    def __imgProcess(self,oriImg):
        self.resize_img = cv2.resize(oriImg,(160,120))
        img_hsv = cv2.cvtColor(self.resize_img,cv2.COLOR_BGR2HSV)
        mask = cv2.inRange(img_hsv,self.lower, self.upper)
        edode_img = cv2.erode(mask,self.element,iterations = 1)
        _,self.contours, hierarchy = cv2.findContours(edode_img, cv2.RETR_LIST, cv2.CHAIN_APPROX_SIMPLE)
    
    def detect(self,oriImg):
        self.count += 1
        # cv2.imwrite('TmpImg/blue/blue{}.jpg'.format(self.count),oriImg)
        self.__imgProcess(oriImg)
        if not len(self.contours):
            return (0,0)
        area = [cv2.contourArea(self.contours[k]) for k in range(len(self.contours))]
        max_idx = np.argmax(np.array(area))
        x, y, w, h = cv2.boundingRect(self.contours[max_idx])
        mid_x = x+w//2
        mid_y = y+h//2
        return (mid_x, mid_y) if (mid_x<=140 and mid_y >=20) else (0, 0) 




class purchasePreClass:
    def __init__(self):
        self.lower = np.array(Config.threshold_version[0])  
        self.upper = np.array(Config.threshold_version[1])
        self.element = cv2.getStructuringElement(cv2.MORPH_RECT, (3, 3))
        self.contours = None
        self.count = 0
        
    def __imgProcess(self,oriImg):
        # self.resize_img = cv2.resize(oriImg,(320,240))
        img_hsv = cv2.cvtColor(oriImg,cv2.COLOR_BGR2HSV)
        mask = cv2.inRange(img_hsv,self.lower, self.upper)
        edode_img = cv2.erode(mask,self.element,iterations = 1)
        imgCanny = cv2.Canny(edode_img,60,60)  #Canny算子边缘检测
        for i in range(110,240):
            imgCanny[i][0]=255
            imgCanny[i][319]=255
        # cv2.imwrite('imgCanny.jpg',imgCanny)
        
        _,self.contours, hierarchy = cv2.findContours(edode_img, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
    
    def detect(self,oriImg):
        # cv2.imwrite('cube.jpg',oriImg)
        self.__imgProcess(oriImg)
        if not len(self.contours):
            return (0,0)
        x = y = 0
        w = 0
        h = 0
        for obj in self.contours:
            area = cv2.contourArea(obj)  #计算轮廓内区域的面积s
            # print('area:',area)
            if (area>=700):
                perimeter = cv2.arcLength(obj,True)  #计算轮廓周长
                approx = cv2.approxPolyDP(obj,0.02*perimeter,True)  #获取轮廓角点坐标
                x, y, w, h = cv2.boundingRect(approx)  #获取坐标值和宽度、高度
                # print('x y w h:',x,y,w,h)
                if(y>=90 and h<110 and w <= 120):
                    # cv2.rectangle(oriImg,(x,y),(x+w,y+h),(0,0,255),2)
                    font = cv2.FONT_HERSHEY_SIMPLEX
                    org = x, y + 25 # 原来是-10
                    fontScale = 0.4
                    color = (0, 244, 10)
                    thickness = 1
                    # oriImg = cv2.putText(oriImg, str(x+w//2) + ' ' + str(y+h//2), 
                    #                 org, font,fontScale, color, thickness, cv2.LINE_AA)
                    # cv2.imwrite("TmpImg/purchase/sample_detectrec{}.jpg".format(self.count),oriImg)
                    self.count +=1
                    break
                else:
                    x=y=0

        return(x+w//2,y+h//2) 



################################export ####################################
sideParkObj = sideParkClass()
purchasePreObj = purchasePreClass()