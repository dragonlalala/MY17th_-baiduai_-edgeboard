'''
Author: your name
Date: 2021-08-16 19:20:00
LastEditTime: 2022-08-21 10:12:17
LastEditors: Please set LastEditors
Description: 
FilePath: \GITBaidu2022\Modules\Vision\ModelTest.py
'''
#!/usr/bin/python
# coding=UTF-8
import os
import cv2
import time
import numpy as np
import multiprocessing

from Modules.Config import *
from Modules.Detector.Detectors import SignDetector, TaskDetector
from Modules.Camera.CameraMulti import frontCamObj, leftCamObj, rightCamObj
from Modules.Lock.Lock import signDetectStop_SHARE, signIndex_SHARE, signX_SHARE, signY_SHARE, signHeight_SHARE, signWidth_SHARE, lock_sign, cruise_or_sign_SHARE

# import multiprocessing
# from lock import *

image_extensions = [".png", ".jpg", ".jpeg"]


def drawRes(frame, results):
    '''
     画锚框
    :param frame:
    :param results:
    :return:
    '''
    res = list(frame.shape)
    # print(results)
    # print('aa')
    for item in results:
        left = item.relative_box[0] * res[1]
        top = item.relative_box[1] * res[0]
        right = item.relative_box[2] * res[1]
        bottom = item.relative_box[3] * res[0]
        start_point = (int(left), int(top))
        end_point = (int(right), int(bottom))
        color = (0, 244, 10)
        thickness = 1
        frame = cv2.rectangle(frame, start_point, end_point, color, thickness)
        font = cv2.FONT_HERSHEY_SIMPLEX
        # org = start_point[0], start_point[1] + 25 # 原来是-10
        org = 20, 120 
        fontScale = 0.4
        frame = cv2.putText(frame, item.name + str(int(left + right) // 2) + ' ' + str(int(top + bottom) // 2) + ' ' + str(int(bottom-top)) + ' ' + str(int(right-left)) + ' ' + str(item.score), 
                                org, font,fontScale, color, thickness, cv2.LINE_AA)
        return frame, int(left + right) // 2, int(top + bottom) // 2, round(item.score,2), int(bottom-top), int(right-left)


class SignModel:
    def __init__(self):
        self.__paused = signDetectStop_SHARE  # 线程标志位,用来表示是否暂停地标检测
        self.__paused.value = False
        self.sign_x = signX_SHARE  # 目标检测到的地标的x坐标
        self.sign_y = signY_SHARE  # 目标检测到的地标的y坐标，现在换作共享变量
        self.sign_index = signIndex_SHARE  # 结果的索引，0为背景
        self.height = signHeight_SHARE # 用于过滤误识别的目标，地标的height最大为62
        self.width = signWidth_SHARE # 用于过滤误识别的目标
        self.sign_score = 0
        self.count = 0
        logging.debug("init sign det")
           

    def __testSign(self):
        # sourcery skip: remove-pass-body, remove-redundant-pass
        '''
        前向车道地面标志动态识别
        :return:
        '''
        sd = SignDetector() # 实例化地标检测对象
        while True:
            if self.__paused.value:
                frontCamObj.img = None # 防止两次识别到地标
                pass
            else:
                try:  # 防止读到空图报错
                    if cruise_or_sign_SHARE.value % 4 == 0:
                        frame = frontCamObj.readForDetect()
                        if frame is not None:
                            # start = time.time()
                        
                            signs, index = sd.detect(frame)
                            # logging.info("sign time:{}".format(time.time()-start))
                            # TODO:查看背景下signs是什么东西
                            if signs != []:
                                lock_sign.acquire()
                                self.sign_index.value = int(signs[0].index)  
                                _, self.sign_x.value, self.sign_y.value, self.sign_score, self.height.value, self.width.value= drawRes(frame.copy(), signs)
                                lock_sign.release()
                                logging.debug("sign index:{}, self.width:{}".format(self.sign_index.value,self.width.value))
                                # if self.height.value < sign_threshold["height_upper"] and sign_threshold["width_lower"] < self.width.value < sign_threshold["width_upper"]: # 用于过滤误识别的目标
                                # cv2.imwrite("TmpImg/sign/{}.jpg".format(self.count),_)
                                # self.count += 1 
                            else:
                                lock_sign.acquire()
                                self.sign_index.value = 0
                                self.sign_x.value = 0
                                self.sign_y.value = 0
                                lock_sign.release()
                                logging.info("sign index:{}".format(self.sign_index.value))
                                
                            # if (sign_threshold["x_lower"] < self.sign_x.value < sign_threshold["x_upper"] 
                            #     and self.sign_y.value > 187 and self.height.value < sign_threshold["height_upper"]
                            #     and sign_threshold["width_lower"] < self.width.value < sign_threshold["width_upper"]):  
                            #     self.sign_index.value = int(signs[0].index)
                            #     logging.info("sign result index:{}".format(self.sign_index.value))
                                
                            #     if self.sign_index.value != 0:  # 如果目标检测的结果不是背景0，那么就退出这个循环 TODO:当满足有10个符合条件才判断成立
                            #         self.pause()
                                    # pass
                        else:
                            logging.info("sign no pic")
                            cv2.waitKey(2)
                    
                except Exception as e:
                    logging.error('raise an error at __testSign in modeltest')
                    logging.error(e)
                    pass

 
    def getPausedFlag(self):
        '''
        description: 获取私有变量self.__paused.value的值
        '''        
        return self.__paused.value

    def start(self):       
        '''
        description:开启线程
        '''
        self.sign_x.value = 0  # 每次检测到，下次开启地标检测时需要赋值为0
        self.sign_y.value = 0  # ~的y坐标
        multiprocessing.Process(target = self.__testSign).start()

    def resume(self):
        '''
        description:恢复线程
        '''
        self.sign_x.value = 0  # 每次检测到，下次开启地标检测时需要赋值为0
        self.sign_y.value = 0  # ~的y坐标
        self.__paused.value = False

    def pause(self):
        '''
        description:暂停线程
        '''
        self.__paused.value = True

    def stop(self):
        '''·
        description:关闭线程
        '''
        pass

# TODO:要放入不同的模型
class TaskModel:
    def __init__(self, src):
        self.x = 0  # 目标中心点的x坐标
        self.y = 0  # 目标中心点的y坐标
        self.result_index = 0  # 结果的索引，0为背景
        self.src = src
        self.td = TaskDetector(src)
        self.count = 0
        self.score = 0
        self.height = 0 # 主要用于打坏人（因为模型容易误识别，所以通过高度来过滤）
        self.width = 0
    
    def initData(self,whichCam):
        # 左边摄像头
        self.x = 320 if whichCam == 2 else 0
        self.y = 0
        self.result_index = 0

    def getXaxisError(self,whichCam,signIndex,task_y,flag_mystery):
        # error为负时，小车是前进的
        # 左边摄像头
        if whichCam == 2: 
            error = leftTargetX[signIndex] - self.x
        else: 
            if signIndex == 7:
                if task_y < fruit_y_thresh:
                    error = self.x - rightTargetX[signIndex][0] # 上层
                else:
                    error = self.x - rightTargetX[signIndex][1] # 下层
            else:
                error = self.x - rightTargetX[signIndex]
        if sign_state[signIndex]["location"]: # 如果需要定位则慢速调整
            speed = 10
        else:
            speed = 20
        if abs(error) <= error_thresh[signIndex]:
            return 0
        if abs(error) <= 100 and error < 0:
            return (-1)*speed
        elif abs(error) <= 100 and error > 0:
            return speed
        else:
            return error//(100//speed)
    
    def testTask(self, camera_name):
        '''
        侧面任务动态识别
        :return:
        '''
        side_image = rightCamObj.readForDetect() if camera_name == 1 else leftCamObj.readForDetect()
        if side_image is not None:
            tasks = self.td.detect(side_image)
            # logging.info('tasks:{}'.format(tasks))
            # tasks:[name:goodperson2 index:7.0 scroe:0.59993898868560791]
            # cv2.imwrite("TmpImg/task/{}_{}.jpg".format(self.src,self.count),side_image)
            if tasks != []:
                _, self.x, self.y, self.score, self.height, self.width= drawRes(side_image.copy(), tasks)
                self.result_index = int(tasks[0].index)
                # cv2.imwrite("TmpImg/task/{}_{}.jpg".format(self.src,self.count),_)
                # self.count += 1
                logging.info('task height:{}'.format(self.height))
                # logging.info('width:{}'.format(self.width))
                return True
            else:
                logging.info("task detect none")
                return False
        else:
            logging.info("task detect no pic")
            return False


##################################export##############################
signDetectObj = SignModel()  # 实例化一个地标检测模型

taskDetectObj = TaskModel(0)  # 实例化一个侧边任务检测模型
taskFruitDetectObj = TaskModel(1) # 实例化一个侧边任务水果检测模型
taskPersonDetectObj = TaskModel(2) # 实例化一个侧边任务人物检测模型
# taskPurchaseDetectObj = TaskModel(3) # 实例化一个侧边任务小方块检测模型