'''
Author: your name
Date: 2022-04-07 11:50:18
LastEditTime: 2022-05-04 00:58:37
LastEditors: Please set LastEditors
Description: 采用多线程来从摄像头获取图像
FilePath: \17th_BAIDUAI_V1\Modules\Vision\Camera.py
'''

#!/usr/bin/python
# coding=UTF-8
"""
import区
"""
import cv2
import threading
import time
from collections import deque
from threading import Lock
from Modules import Config

"""
类定义区
"""
class CameraClass:
    def __init__(self, src=0, shape=[480, 320]):
        # sourcery skip: default-mutable-arg
        '''
        description: 摄像头初始化函数，创建对象时自动调用
        param {*} src 摄像头编号
        param {*} shape 获取的图像的尺寸
        '''   
        self.src = src
        self.stream = cv2.VideoCapture(src)
        # 设置图像编码格式
        self.stream.set(cv2.CAP_PROP_FOURCC, cv2.VideoWriter_fourcc('M', 'J', 'P', 'G'))
        # 设置从摄像头获取的图像尺寸
        self.stream.set(cv2.CAP_PROP_FRAME_WIDTH, shape[0])
        self.stream.set(cv2.CAP_PROP_FRAME_HEIGHT, shape[1])
        # 打印输出摄像头的图像编码格式
        print(self.src,"".join([chr((int(self.stream.get(cv2.CAP_PROP_FOURCC)) >> 8 * i) & 0xFF) for i in range(4)]))
        # 创建锁对象，主要用于队列操作
        self.lock = Lock()
        # 创建双端对垒
        self.dequer = deque([], 4)
        # 获取摄像头图像线程的暂停重启标志位
        self.__paused = False
        self.img = None
        # 摄像头预热
        for _ in range(10):  
            (grabbed, frame) = self.stream.read()

    def start(self):
        """
        摄像头的一个线程，图片供巡航和检测用
        """
        self.__paused =False
        threading.Thread(target=self.__update, args=()).start()

    def __update(self):  # sourcery skip: remove-empty-nested-block, remove-redundant-if, remove-redundant-pass
        """
        如果__paused的话，就停止读图片
        """
        while True:
            if self.__paused:
                self.dequer.clear()
                pass
            else:
                self.grabbed, self.frame = self.stream.read()
                if self.grabbed:
                    print(self.src,':yes')
                    with self.lock:
                        if len(self.dequer) == 4:
                           self.dequer.popleft()
                        else:
                            self.dequer.append(self.frame)
                else:
                    print(self.src,'wrong')


    def read(self):
        if len(self.dequer) == 0:
            return None
        with self.lock:
            print('queue len:',len(self.dequer))
            self.img = self.dequer.popleft().copy()
        return self.img

    def pause(self):
        self.__paused = True

    def resume(self):
        self.__paused = False

#################################export####################################### 

leftCamObj = CameraClass(Config.left_cam,[320,240])  # 左边的摄像头
frontCamObj = CameraClass(Config.front_cam, [320, 240])   # 前边的摄像头
rightCamObj = CameraClass(Config.right_cam, [320, 240])   # 右边的摄像头


        
