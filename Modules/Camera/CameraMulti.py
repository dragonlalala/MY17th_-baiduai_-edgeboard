'''
Author: your name
Date: 2022-05-03 09:32:18
LastEditTime: 2022-07-19 00:39:45
LastEditors: Please set LastEditors
Description: 采用多进程来从摄像头获取图像
FilePath: \17th_BAIDUAI_V1\Modules\Camera\CameraMulti.py
'''
#!/usr/bin/python
# coding=UTF-8
"""
import区
"""
import cv2
import threading
import time
import multiprocessing
from Modules.Config import logging
from Modules import Config
from Modules.Lock.Lock import frontCamStop_SHARE,leftCamStop_SHARE,rightCamStop_SHARE

"""
全局变量区
"""
# 控制是否获取图片的共享变量，对应src
flagStopList = [frontCamStop_SHARE,rightCamStop_SHARE,leftCamStop_SHARE]
# 图像队列
frontQueueDetect = multiprocessing.Queue(maxsize=4)
frontQueueCruise = multiprocessing.Queue(maxsize=4)
rightQueue = multiprocessing.Queue(maxsize=4)
leftQueue = multiprocessing.Queue(maxsize=4)
# frontQueue = multiprocessing.Manager().Queue(maxsize=4)
# rightQueue = multiprocessing.Manager().Queue(maxsize=4)
# leftQueue = multiprocessing.Manager().Queue(maxsize=4)

"""
类定义区
"""
class CameraClass:
 
    def __init__(self, __queue_detect, __queue_cruise = None, src=0, shape=[480, 320]):
        # sourcery skip: default-mutable-arg
        '''
        description: 摄像头初始化函数，创建对象时自动调用
        param {*} queue 对应摄像头的图像队列
        param {*} src 摄像头编号
        param {*} shape 获取的图像的尺寸
        '''   
        self.src = src
        self.stream = cv2.VideoCapture(src)
        # 设置图像编码格式
        self.stream.set(cv2.CAP_PROP_FOURCC, cv2.VideoWriter_fourcc('M', 'J', 'P', 'G'))
        # 设置从摄像头获取的图像大小
        self.stream.set(cv2.CAP_PROP_FRAME_WIDTH, shape[0])
        self.stream.set(cv2.CAP_PROP_FRAME_HEIGHT, shape[1])
        # 打印信息指明摄像头的图像编码格式
        logging.info(str(self.src)+"".join([chr((int(self.stream.get(cv2.CAP_PROP_FOURCC)) >> 8 * i) & 0xFF) for i in range(4)]))
        # 图像队列
        self.queue_detect = __queue_detect
        self.queue_cruise = __queue_cruise
        self.img = None
        # 预热摄像头
        for _ in range(5):  
            (grabbed, frame) = self.stream.read()

    def start(self):
        """
        摄像头的一个进程，图片供巡航和检测用
        """
        flagStopList[self.src].value = False
        multiprocessing.Process(target = self.__update).start()

    def __update(self):  # sourcery skip: remove-empty-nested-block, remove-redundant-if, remove-redundant-pass
        """
        进程的主要函数，一直在从摄像头获取图像
        """
        while True:
            if flagStopList[self.src].value == True:
                # self.__clear()                                    # 清空图像队列
                # if count%10000 == 0 and self.src == 2:
                #     logging.info('cam{}:closed'.format(self.src))
                # count += 1
                pass
            else:
                self.grabbed, self.frame = self.stream.read()   # 从摄像头获取图像
                if self.grabbed:
                    if self.queue_detect.full():
                        try:
                            _ = self.queue_detect.get(False,0.01)
                        except Exception:
                            logging.info('cam{}:queue_detect full but get none'.format(self.src))
                    self.queue_detect.put(self.frame)                      # 压进队列

                    if self.queue_cruise is not None:
                        if self.queue_cruise.full():
                            try:
                                _ = self.queue_cruise.get(False,0.01)
                            except Exception:
                                logging.info('cam{}:queue_cruise full but get none'.format(self.src))
                        self.queue_cruise.put(self.frame.copy())                      # 压进队列 
                else:
                    logging.info('cam{}:get wrong'.format(self.src))


    def readForDetect(self):
        """
        从图像队列获得图像，供外部使用
        """
        if self.queue_detect.empty():
            logging.info('cam{}:queue_detect empty'.format(self.src))
            return self.img # 队列为空就使用最近的那张
        self.img = self.queue_detect.get()
        return self.img

    def readForCruise(self):
        """
        从图像队列获得图像，供外部使用
        """
        if self.queue_cruise.empty():
            # logging.info('cam{}:queue_cruise empty'.format(self.src))
            return self.img # 队列为空就使用最近的那张
        self.img = self.queue_cruise.get()
        return self.img
    

    def pause(self):
        """
        暂停摄像头进程，供外部使用
        """
        flagStopList[self.src].value = True
        logging.info('cam{}:pause'.format(self.src))
        self.__clear()
        logging.info('cam{}:clear finish!'.format(self.src))

    def resume(self):
        """
        重启摄像头进程，供外部使用
        """
        self.img = None # 防止读到前一次任务的图片
        flagStopList[self.src].value = False
        logging.info('cam{}:resume'.format(self.src))

 
    
    def __clear(self):
        """
        清空队列，供内部使用
        """
        # while(not self.queue_detect.empty()):
        #     _ = self.queue_detect.get()
        #     logging.info('cam{}:queue_detect clearing'.format(self.src))   
        # if self.queue_cruise is not None:
        #     while(not self.queue_cruise.empty()):
        #         _ = self.queue_cruise.get()
        #         logging.info('cam{}:queue_cruise clearing'.format(self.src))  
        while(not self.queue_detect.empty()):
            try:
                _ = self.queue_detect.get(False,0.01)
                logging.info('cam{}:queue_detect clearing'.format(self.src))   
            except Exception:
                logging.info('cam{}:queue_detect clearing but get none'.format(self.src))   
        if self.queue_cruise is not None:
            while(not self.queue_cruise.empty()):
                try:
                    _ = self.queue_cruise.get(False,0.01)
                    logging.info('cam{}:queue_cruise clearing'.format(self.src))  
                except Exception:
                    logging.info('cam{}:queue_cruise clearing but get none'.format(self.src))   
    def clearDetectQueue(self):
        """
        清空地标检测队列
        """
        while(not self.queue_detect.empty()):
            _ = self.queue_detect.get()
            logging.info('cam{}:queue_detect clearing'.format(self.src)) 

#################################export####################################### 
leftCamObj = CameraClass(leftQueue,None,Config.left_cam,[320,240])        # 左边的摄像头
frontCamObj = CameraClass(frontQueueDetect,frontQueueCruise,Config.front_cam, [320, 240])   # 前边的摄像头
rightCamObj = CameraClass(rightQueue,None,Config.right_cam, [320, 240])   # 右边的摄像头


        
