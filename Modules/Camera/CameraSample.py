'''
Author: your name
Date: 2022-04-07 11:50:18
LastEditTime: 2022-05-03 09:22:31
LastEditors: Please set LastEditors
Description: 普通的摄像头类，即只初始化了摄像头，需要图像时再调用self.stream.read()获取。
FilePath: \17th_BAIDUAI_V1\Modules\Vision\Camera.py
'''

#!/usr/bin/python
# coding=UTF-8
import cv2
import threading
import time
from Modules import Config 


class CameraClass:
    def __init__(self, src=0, shape=[480, 320]):
        # sourcery skip: default-mutable-arg
        self.src = src
        self.stream = cv2.VideoCapture(src)
        self.stream.set(cv2.CAP_PROP_FOURCC, cv2.VideoWriter_fourcc('M', 'J', 'P', 'G'))
        self.stream.set(cv2.CAP_PROP_FRAME_WIDTH, shape[0])
        self.stream.set(cv2.CAP_PROP_FRAME_HEIGHT, shape[1])
        print(src,":".join([chr((int(self.stream.get(cv2.CAP_PROP_FOURCC)) >> 8 * i) & 0xFF) for i in range(4)]))
        self.__paused = False
        for _ in range(5):  # warm up the camera
            (grabbed, frame) = self.stream.read()




#################################export####################################### 

leftCamObj = CameraClass(Config.left_cam,[320,240])  # 左边的摄像头
frontCamObj = CameraClass(Config.front_cam, [320, 240])   # 前边的摄像头
rightCamObj = CameraClass(Config.right_cam, [320, 240])   # 右边的摄像头
