# sourcery skip: use-fstring-for-concatenation
'''
Author: your name
Date: 2022-04-10 16:02:53
LastEditTime: 2022-08-21 10:13:08
LastEditors: Please set LastEditors
Description: In User Settings Edit
FilePath: \百度创意_多线程\config.py
'''

#!/usr/bin/python
# coding=UTF-8

# 设备名称
front_cam = 0  # /dev/video0
right_cam = 1
left_cam = 2
serial_name = {
    0:"/dev/ttyUSB0",  # linux，一般只插入一个，就是ttyUSB0
    1:"COM8"           # window电脑，需要查看设备管理器上的COM对应的是多少
}

# 颜色阈值选择
color_threshold = {
    1: ([84,83,106],[102,177,255]),  # 蓝色
    # 1: ([99,57,142],[108,167,245]),  # 蓝色   广技师
    # 2: ([46,52,97],[65,184,244]),    # 绿色
    2: ([40,28,29],[89,158,244]),
    3: ([5,154,85],[23,255,255]),    # 橙色
    4: ([168,64,52],[179,229,249]),  # 红色
    5: ([22,128,49],[29,255,255])    # 黄色
    # 5: ([22,128,49],[35,255,255])    # 黄色   广技师
}
threshold_version = color_threshold[2]

sign_state = {
    #location指是否需要精确定位，若是则行进速度为10，否则20
    0:{"label":"背景类",   "accomplished":True, "location":False, "intercept":30},
    1:{"label":"文化交流", "accomplished":False, "location":False, "intercept":33},
    2:{"label":"放歌友谊", "accomplished":False, "location":False, "intercept":30},
    3:{"label":"采购货物", "accomplished":False, "location":True, "intercept":25},  
    4:{"label":"守护丝路", "accomplished":False, "location":True, "intercept":24},
    5:{"label":"以物易物", "accomplished":False, "location":True, "intercept":25},
    6:{"label":"翻山越岭", "accomplished":True, "location":False, "intercept":30},
    7:{"label":"夹水果",   "accomplished":False, "location":True, "intercept":25}
}
sign_threshold = {
    "x_lower": 95,
    "x_upper": 230,
    "y_slow": 50,
    "height_upper": 115,
    "width_lower":20,
    "width_upper": 100
}

IS_FRIENDSHIP_OPENLOOP = 0 # 侧方停车方式指定
FRIENDSHIP_MOVETIME = 1.1
fruit_y_thresh = 125 # 水果区分上下层的y的阈值

# 侧边任务的targetX
leftTargetX ={
    1: 155,             # 城池期望X坐标,173
    2: 120,             # 侧方停车期望X坐标
    4: 160,             # 左摄像头打坏人期望X坐标
    8: 70               # 神秘任务
}
rightTargetX = {
    3: 252,             # 夹小方块期望X坐标(模型的话就250)，省赛256
    4: 145,             # 右摄像头打坏人期望X坐标
    5: 93,              # 放小方块期望X坐标
    7: [282,252]              # 夹水果期望X坐标,[250,222]是正中心(第一个数是上层的),target建议在此基础上加3,国赛[280,249]
}
error_thresh = {        # 满足停车条件的误差允许范围
    1: 40,
    2: 10,
    3: 5,
    4: 5,
    5: 5,
    7: 5
}
# 每个任务设置不同的timeout
timeout_dict = {        # 单位:second
    1: 6.5,             # 文化交流(举旗)
    2: 5,               # 放歌友谊(侧方停车)
    3: 6,               # 采购货物(夹小方块)
    4: 5,               # 守护丝路(打坏人)
    5: 5,               # 交易(放小方块)
    6: 5,               # 翻山越岭(过跷跷板)
    7: 5                # 交易(夹水果)
}

# 日志输出配置
import logging
# logging.basicConfig函数对日志的输出格式及方式做相关配置
logging.basicConfig(level = logging.DEBUG, format = '%(levelname)s: %(message)s')

model_prefix = "/home/root/workspace/17BaiduAI_V4/Model/"  # 模型路径前缀

# mission config
# one more for background
MISSION_NUM = 7 # 必须大于以下几个list中的最大索引

mission_label_list = {
    0: "background",
    1: "alamutu",     # 阿拉木图
    2: "dunhuang",    # 敦煌
    3: "friendship",  # 放歌友谊
    4: "jstdb",       # 君士坦丁堡
    5: "purchase"     # 购买(夹小方块)
}

mission_fruit_label_list = {
    0: "background",
    1: "grape",   # 葡萄
    2: "kiwi",  # 奇异果
    3: "litchi",  # 荔枝
    4: "loquat", # 枇杷
    5: "rice", # 水稻
    6: "walnut" # 核桃
}
mission_person_label_list = {
    0: "background",
    1: "badperson",   # 1号坏人
    2: "badperson2",  # 2号坏人
    3: "goodperson",  # 1号好人
    4: "goodperson2" # 2号好人
}

# sign config
sign_list = {
    0: "background",
    1: "fortress",    # 城池
    2: "friendship",  # 侧方停车
    3: "purchase",    # 夹方块
    4: "target",      # 打坏人
    5: "trade",       # 交易(放小方块，夹大方块)
    6: "seesaw"       # 翻山越岭
}

# cruise model
cruise = {
    "model": model_prefix + "CruiseModel200e",
}

# sign models
sign = {
    "model": model_prefix + "SignModel",
    "threshold": 0.45,
    "label_list": sign_list,
    "class_num": 7
}

# 模型选择
color_list = {
    1: "blue",   # 蓝色小方块,target_x:260
    2: "green",  # 绿色小方块,target_x:261(易识别黄色)
    3: "orange", # 橙色小方块,target_x:261(易识别黄色)
    4: "red",    # 红色小方块,target_x:261
    5: "yellow"  # 黄色小方块,target_x:261(易识别橙色)
}
model_version = color_list[1] # 只能选1，因为旗帜扩充数据集训练得到的模型只有1才是！！

# task model
task = [{"model": model_prefix +"TaskModel/"+model_version,"threshold": 0.8,"label_list": mission_label_list},
        {"model": model_prefix +"FruitModel","threshold": 0.15,"label_list": mission_fruit_label_list},
        {"model": model_prefix +"PersonModel","threshold": 0.35,"label_list": mission_person_label_list}]
