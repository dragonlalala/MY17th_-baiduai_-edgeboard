#!/usr/bin/python
# coding=UTF-8
import sys
# sys.path.append("/home/root/workspace/17BaiduAI_V4") 
import cv2
import numpy as np
import time
import os
import ctypes
import multiprocessing
from Modules.Camera.CameraMulti import frontCamObj
# from Modules.Detector.Detectors import Cruiser
from Modules.Detector import PredictorWrapper
from Modules import Config
from Modules.Serial.Serial import serialObj
from Modules.Lock.Lock import cruiseStop_SHARE,cruiseSendMidErrorFlag_SHARE,cruiseMidError_SHARE,lock_use_fpga,cruise_or_sign_SHARE
from Modules.Config import logging
"""
camera cruise detect 开启进程
"""

cnn_args = {
    "shape": [1, 3, 128, 128],
    "ms": [125.5, 0.00392157]
}

class DriverClass:
    args = cnn_args

    def __init__(self):
        # 进程标志位
        self.__paused = cruiseStop_SHARE
        # 进程是否发送偏差给下位机的标志位
        self.sendFlag = cruiseSendMidErrorFlag_SHARE
        self.mid_error_share = cruiseMidError_SHARE
        # 原图像
        self.oriImg = None
        # 模型加载
        self.predictor = PredictorWrapper.PaddleLitePredictor()
        self.predictor.load(Config.cruise["model"])
        hwc_shape = list(self.args["shape"])
        hwc_shape[3], hwc_shape[1] = hwc_shape[1], hwc_shape[3]
        self.buf = np.zeros(hwc_shape).astype('float32')
        self.size = self.args["shape"][2]
        self.means = self.args["ms"]
        self.lower = np.array([22, 43, 46])  # 黄色的下阈值  
        self.upper = np.array([34, 255, 255])
        self.element = cv2.getStructuringElement(cv2.MORPH_RECT, (3, 3))
    
    # CNN网络的图片预处理
    def image_normalization(self, image):
        #####
        image = cv2.resize(image, (self.size, self.size))
        #####
        image = image.astype(np.float32)
        # if platform.system() == "Windows":
        #     image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
        #     image = image.transpose((2, 0, 1))  # HWC to CHW
        # 转换成BGR
        img = (image - self.means[0]) * self.means[1]
        img = img.reshape([1, 3, 128, 128])

        return img

    # CNN网络预测
    def infer_cnn(self, image):
        data = self.image_normalization(image)
        lock_use_fpga.acquire()
        self.predictor.set_input(data, 0)
        self.predictor.run()
        out = self.predictor.get_output(0)
        lock_use_fpga.release()
        # print(type(out))
        return np.array(out)[0][0]

    def __driveAuto(self):    # sourcery skip: remove-pass-body
        '''
        description: 巡航的主程序
        param {*}
        return {*}
        '''        
        count = 0
        img_count = 25716
        while True:
            if self.__paused.value:
                pass
            else:
                # try:
                count  = count + 1
                
                self.oriImg  = frontCamObj.readForCruise()
                if self.oriImg is not None:
                    #####
                    # img_resize = cv2.resize(self.oriImg,(128,128)) #0.4ms
                    # img_hsv = cv2.cvtColor(img_resize,cv2.COLOR_BGR2HSV) # 1.1ms->0.3ms
                    # mask = cv2.inRange(img_hsv,self.lower,self.upper)    # 1ms ->0.3ms 
                    # imgdilate = cv2.dilate(mask, self.element)  # 膨胀                     #0.4ms
                    # self.img = cv2.morphologyEx(imgdilate, cv2.MORPH_CLOSE, self.element)  # 先腐蚀后膨胀操作，原为腐蚀操作 #0.4ms
                    ####
                    start = time.time()
                    # cv2.imwrite('TmpImg/sign/'+str(count)+'.jpg',self.oriImg)
                    cruise_or_sign_SHARE.value += 1
                    ######
                    angle = self.infer_cnn(self.oriImg)
                    ######
                    logging.info('cruise time:{}'.format(time.time()-start))
                    mid_error = int(angle * 100)
                    if count%10 == 0:
                        logging.info('mid_error:{}'.format(mid_error))
                    
                    # # if count :
                    # if count % 5 == 0:
                    #     img_count += 1
                    #     cv2.imwrite('./d1/'+str(img_count)+'.jpg',self.oriImg)
                    #     imgpath = 'd1/{}.jpg'.format(img_count)
                    #     with open('./d1.txt', 'a') as f:
                    #         f.writelines(imgpath + '\t' + str(mid_error/100) + '\n')
                    # 限幅
                    if -100 <= mid_error <=100:
                        # 发送偏差给下位机
                        self.mid_error_share.value = mid_error
                        if self.sendFlag.value:
                            serialObj.writeData([0xAA,0xC1,100,100,int(mid_error+100),0xFF])
                else:
                    logging.info('cruise no pic')
                    #  time.sleep(0.03)

                # except Exception as e:
                #     print('e', e)
           
    def start(self):
        self.__paused.value = False
        multiprocessing.Process(target = self.__driveAuto).start()
    
    def pause(self):
        self.__paused.value= True
        logging.info('cruise pause')
    
    def resume(self):
        self.__paused.value = False
        logging.info('cruise resume')

    def getPausedFlag(self):
        return self.__paused.value

#############################export###########################           
cruiseObj = DriverClass()

# if __name__ == '__main__':
#     cruiseObj.start()
