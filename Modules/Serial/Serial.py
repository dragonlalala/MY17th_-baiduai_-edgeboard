'''
Author: your name
Date: 2022-04-07 11:50:18
LastEditTime: 2022-04-28 16:53:28
LastEditors: Please set LastEditors
Description: In User Settings Edit
FilePath: \17th_BAIDUAI_V1\Modules\Serial\Serial.py
'''

import serial
import time
import platform
# from threading import Lock
import binascii
from Modules.Lock.Lock import lock_serial
from Modules.Config import serial_name

"""
通信协议：
注意：每个数据帧为0-255/0x00-0xff。
帧头：0xAA
第1个数据帧：代表类型
0xC1:代表巡航
0xAC:代表任务(下位机动作指令)
0xDE:代表对下位机xyz方向速度的修改
第2、3、4个数据帧：
若为巡航，则第2、3、4个数据帧分别代表X,Y,Z方向的currentPose，X,Y都为0，Z为上位机寻线得到的偏差+100加100是为了区间[-100,100]投影到[0,200]。
若为任务，则第二个数据帧为任务的序号，第三个数据帧为相应任务的参数(距离/角度)，任务序号如下
    1: "fortress",    # 城池
    2: "friendship",  # 侧方停车
    3: "purchase",    # 夹方块
    4: "target",      # 打坏人
    5: "trade"        # 交易(放小方块，夹大方块)
若为速度修改。则第2、3、4个数据帧分别代表X,Y,Z方向的targetSpeed+100,加100是为了区间[-100,100]投影到[0,200]。
帧尾：0xFF
"""
"""
samples:
巡航发送偏差:serialObj.writeData([0xAA,0xC1,0,0,mid_error+100,0xFF])
城池举旗任务发送指令：serialObj.writeData([0xAA,0xAC,1,1,0,0xFF])第1个代表城池举旗任务，第二个1代表敦煌
X方向速度修改指令：serialObj.writeData([0xAA,0xDE,10+100,0,0,0xFF])修改X方向速度为10
"""

class SerialClass:
    def __init__(self, lock, port):                             # 传入串口名称
        self.lock = lock
        __portx = port
        __bps = 115200                                          # 波特率
        self.__serial = serial.Serial(__portx, __bps, timeout=1, parity=serial.PARITY_NONE, stopbits=1)

        time.sleep(1)

    def writeData(self, data2send = None):    # 帧头 任务标志 数据长度 x y z 帧尾
        if data2send is None:
            data2send = [0xAA, 0, 0, 0, 0, 0xFF]
        self.lock.acquire()
        self.__serial.write(data2send)                          # 发送列表数据
        self.__serial.flush()                                   # 等待发送完成
        self.lock.release()

    def readData(self):
        """
        下位机发过来任务完成的标志位和计步完成的标志位，数据帧为 0xaa,0x??,0xff.??代表是哪个标志位。
        任务完成：0xaf(task finish)； 计步完成：0xcf(count finish)
        :return:
        """
        result = self.__serial.read(3)                          # 读3个字节 形式为b'\xaa\xc1\xff'
        aa = binascii.b2a_hex(result)                           # 去掉\x ,形式为b'aac1ff'
        bb = aa.decode()                                        # 字节流转化为字符串，形式为 aac1ff
        if bb[:2] == 'aa' and bb[4:6] == 'ff':                 # 数据帧首尾检验
            return bb[2:4]                                      # 返回标志位帧
        

    def getSizeOfOutputBuffer(self):
        return self.__serial.out_waiting
    
    def cleanInputBuffer(self):
        self.__serial.flushInput() # 清除输入缓冲区数据
        
#################################export##############################################

# note:window为“COM+数字”,linux上有ttyUSB和ttyPS
if platform.system() == "Linux":  
    serialObj = SerialClass(lock_serial, serial_name[0])         # 实例化一个Linux串口对象
else:
    serialObj = SerialClass(lock_serial, serial_name[1])         # 实例化一个Windows串口对象