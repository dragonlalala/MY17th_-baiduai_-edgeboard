#!/usr/bin/python
# coding=UTF-8
# from camera import *
import cv2
import numpy as np
from Modules.Detector import PredictorWrapper
from Modules import Config
from Modules.Lock.Lock import lock_use_fpga
import time

ssd_args = {
    "shape": [1, 3, 240, 320], # [1,C,H,W]
    "ms": [127.5, 0.007843]
}
cnn_args = {
    "shape": [1, 3, 128, 128],
    "ms": [125.5, 0.00392157]
}

# class Cruiser:
#     args = cnn_args

#     def __init__(self):
#         # if platform.system() == "Windows":
#         #     self.predictor = PaddlePaddlePredictor()
#         # else:
#         self.predictor = PredictorWrapper.PaddleLitePredictor()
#         self.predictor.load(Config.cruise["model"])
#         hwc_shape = list(self.args["shape"])
#         hwc_shape[3], hwc_shape[1] = hwc_shape[1], hwc_shape[3]
#         self.buf = np.zeros(hwc_shape).astype('float32')
#         self.size = self.args["shape"][2]
#         self.means = self.args["ms"]

#     # CNN网络的图片预处理
#     def image_normalization(self, image):
#         image = cv2.resize(image, (self.size, self.size))

#         image = image.astype(np.float32)
#         # if platform.system() == "Windows":
#         #     image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
#         #     image = image.transpose((2, 0, 1))  # HWC to CHW
#         # 转换成BGR
#         img = (image - self.means[0]) * self.means[1]
#         img = img.reshape([1, 3, 128, 128])

#         return img

#     # CNN网络预测
#     def infer_cnn(self, image):
#         data = self.image_normalization(image)
#         self.predictor.set_input(data, 0)

#         self.predictor.run()
#         out = self.predictor.get_output(0)
#         # print(type(out))
#         return np.array(out)[0][0]

#     def cruise(self, image):
#         res = self.infer_cnn(image)
#         # print(res)
#         return res
        
def ssd_preprocess(args, src):
    shape = args["shape"]
    # src shape(240,320,3)HWC
    img = src.copy()
    img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB) #  2ms
    img = img.astype(np.float32)
    img -= 127.5
    img *= 0.007843  #5ms
    # img = cv2.dnn.blobFromImage(cv2.resize(src, (300, 300)), 0.007843, (300, 300), 127.5,swapRB=True)
    z = np.zeros((1, shape[2], shape[3], 3)).astype(np.float32)
    z[0, 0:img.shape[0], 0:img.shape[1] + 0, 0:img.shape[2]] = img
    z = z.reshape(1, 3, shape[2], shape[3])
    return z


def infer_ssd(predictor, image):
    data = ssd_preprocess(ssd_args, image) # 12ms
    # print('data.shape', data.shape)
    lock_use_fpga.acquire()
    predictor.set_input(data, 0)
    predictor.run() # 28ms
    out = predictor.get_output(0)
    lock_use_fpga.release()
    # print('type(out)',type(out)) #class'paddlelite.Tensor'
    # print('out.shape()', out.shape())
    if out.shape()[1] != 6:
        return np.array([[0.,0.01,0.01,0.01,0.01,0.01]])

    return np.array(out)


# score较高
def is_sign_valid(o):
    return o[1] > Config.sign["threshold"]


def is_task_valid(o, src):
    return o[1] > Config.task[src]["threshold"]


class DetectionResult:
    def __init__(self):
        self.index = 0
        self.score = 0
        self.name = ""
        self.relative_box = [0, 0, 0, 0]
        self.relative_center_y = -1

    def __repr__(self):
        return "name:{} index:{} scroe:{}".format(self.name, self.index, self.score)

    def get_index(self):
        return self.index


# should be a class method?
def res_to_detection(item, label_list, frame):
    detection_Obj = DetectionResult()
    detection_Obj.index = item[0]
    detection_Obj.score = item[1]
    detection_Obj.name = label_list[item[0]]
    detection_Obj.relative_box = item[2:6]
    detection_Obj.relative_center_y = (item[1] + item[3]) / 2
    # print("res_to_detection:{}  {}".format(detection_Obj.name, detection_Obj.score))
    return detection_Obj


class SignDetector:
    def __init__(self):
        self.predictor = PredictorWrapper.PaddleLitePredictor()
        self.predictor.load(Config.sign["model"])
        self.label_list = Config.sign["label_list"]
        self.class_num = Config.sign["class_num"]

    def detect(self, frame):
        res = infer_ssd(self.predictor, frame)
        # print("res=",res)
        res = np.array(res)
        labels = res[:, 0]
        scores = res[:, 1]
        # only one box for one class
        maxscore_index_per_class = [-1 for _ in range(self.class_num)]
        maxscore_per_class = [-1 for _ in range(self.class_num)]
        for count, (label, score) in enumerate(zip(labels, scores)):
            if score > maxscore_per_class[int(label)]:
                maxscore_per_class[int(label)] = score
                maxscore_index_per_class[int(label)] = count
        maxscore_index_per_class = [i for i in maxscore_index_per_class if i != -1]
        res = res[maxscore_index_per_class, :]
        # print(res)
        blow_center = 0
        blow_center_index = -1
        index = 0
        results = []
        #  print('my_res', res)
        for item in res:
            if is_sign_valid(item):
                detect_res = res_to_detection(item, self.label_list, frame)
                # print('detect_res=', detect_res)
                results.append(detect_res)
                if detect_res.relative_center_y > blow_center:
                    blow_center_index = index
                    blow_center = detect_res.relative_center_y
                index += 1
        return results, blow_center_index


class TaskDetector:
    def __init__(self, src=0):
        self.src = src
        self.predictor = PredictorWrapper.PaddleLitePredictor()
        self.predictor.load(Config.task[self.src]["model"])
        self.label_list = Config.task[self.src]["label_list"]


    # only one gt for one label
    def detect(self, frame):
        nmsed_out = infer_ssd(self.predictor, frame)
        # print("nmsed_out=",nmsed_out)
        max_indexes = [-1 for _ in range(Config.MISSION_NUM)]
        max_scores = [-1 for _ in range(Config.MISSION_NUM)]
        # print("max_scores=",max_scores)
        predict_label = nmsed_out[:, 0].tolist()
        predict_score = nmsed_out[:, 1].tolist()
        for count, (label, score) in enumerate(zip(predict_label, predict_score)):
            if score > max_scores[int(label)] and score > Config.task[self.src]["threshold"]:
                max_indexes[int(label)] = count
                max_scores[int(label)] = score
                # print("max_scores=",max_scores)
                # print("max_indexes=",max_indexes)
        selected_indexes = [i for i in max_indexes if i != -1]
        # print('selected_indexes:{}'.format(selected_indexes))
        task_index = selected_indexes
        # print('task_index:{}'.format(task_index))
        res = nmsed_out[task_index, :]
        # print('res:{}'.format(res))
        results = []
        for item in res:
            if is_task_valid(item, self.src):
                results.append(res_to_detection(item, self.label_list, frame))
        # print('results:{}'.format(results))
        return results

class TaskDetectorOnPC:
    def __init__(self, src = 0):
        self.src = src
        self.predictor = PredictorWrapper.PaddlePaddlePredictor()
        self.predictor.load(Config.task[self.src]["model"])
        self.label_list = Config.task[self.src]["label_list"]


    # only one gt for one label
    def detect(self, frame):
        nmsed_out = infer_ssd(self.predictor, frame)
        # print("nmsed_out=",nmsed_out)
        max_indexes = [-1 for _ in range(Config.MISSION_NUM)]
        max_scores = [-1 for _ in range(Config.MISSION_NUM)]
        # print("max_scores=",max_scores)
        predict_label = nmsed_out[:, 0].tolist()
        predict_score = nmsed_out[:, 1].tolist()
        for count, (label, score) in enumerate(zip(predict_label, predict_score)):
            if score > max_scores[int(label)] and score > Config.task[self.src]["threshold"]:
                max_indexes[int(label)] = count
                max_scores[int(label)] = score
        selected_indexes = [i for i in max_indexes if i != -1]
        task_index = selected_indexes
        res = nmsed_out[task_index, :]
        results = []
        for item in res:
            if is_task_valid(item, self.src):
                results.append(res_to_detection(item, self.label_list, frame))
        return results