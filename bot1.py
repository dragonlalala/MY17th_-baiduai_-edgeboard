#!/usr/bin/python
# coding=UTF-8
import sys
import time
# from Modules.Config import logging
# from Modules.Config import IS_FRIENDSHIP_OPENLOOP
# from Modules.Config import timeout_dict
# from Modules.Config import sign_threshold
# from Modules.Config import sign_state
# from Modules.Config import fruit_y_thresh
# from Modules.Cruise.CruiseCpp import *
from Modules.Cruise.CruiseModel import *
from Modules.Vision.ModelTest import *
from Modules.Vision.Preprocess import sideParkObj,purchasePreObj
from Modules.Serial.Serial import serialObj
from Modules.Lock.Lock import lock_sign  
import Modules.Watch.Watch

"""
camera cruise detect 开启进程
"""

frontCamObj.start()
cruiseObj.start()
signDetectObj.start()
leftCamObj.start()
time.sleep(0.01)
leftCamObj.pause()
rightCamObj.start()
time.sleep(0.01)
rightCamObj.pause()
"""
全局变量
"""
bad_target_num = 0  #友谊放歌标号，识别后置一不再进入该模型
flag_slow = True      # 用于防止多次进入发送在地标前减速的if里面
flag_target_left = True 
fruit_2d_list = [] # 用于存放水果index和y坐标的二维列表
left_cam_sign = [1,2] # 记录需要开启左摄像头的地标index
right_cam_sign= [3,5,7] # 记录需要开启右摄像头的地标index
num_of_target = 0       # 记录现在是第几个打坏人 
num_of_fortress = 0
order_num = 0
flag_mystery = False # 是否做神秘任务
flag_2_mystery = False # 是否做完神秘任务
taskObj_dict = {1:taskDetectObj, 
                2:taskDetectObj, 
                3:taskDetectObj, # taskPurchaseDetectObj
                4:taskPersonDetectObj, 
                5:taskFruitDetectObj, 
                6:taskFruitDetectObj,
                7:taskFruitDetectObj} # 根据地标index选择对应的模型

'''
description: 侧边停车车身姿态调整
param {*} is_friendship_openloop friendship 任务是否开环，闭环该值为0 
return {*}
'''
def sideParking(is_friendship_openloop = 1): # 现在默认是1
    # 20的速度，向左平移
    serialObj.writeData([0xAA, 0xC1, 100, 100-100, 100, 0xFF])
    logging.info("friendship openloop")
    # 延时3秒
    time.sleep(FRIENDSHIP_MOVETIME)
    serialObj.writeData([0xAA, 0xC1, 100, 100, 100, 0xFF])
    # 校正姿态
    frontCamObj.resume()
    time.sleep(0.4)
    target_pos =(80,60)
    last_pos = (0,0)
    curr_pos = (0,0)
    while True:
        img = frontCamObj.readForDetect()
        if img is not None:
            pos_res = sideParkObj.detect(img.copy())
            logging.info("sidePark_pos_res:{}".format(pos_res))
            if(pos_res == (0,0)):
                curr_pos = last_pos
            else:
                curr_pos = pos_res
                last_pos = curr_pos
            error_x = curr_pos[0] - target_pos[0]
            error_y = curr_pos[1] - target_pos[1]
            # 限制范围
            if(abs(error_x)<=2):
                error_x = 0
            if(abs(error_y)<=10):
                error_y = 0
            if(error_x==0 ):
                serialObj.writeData([0xAA, 0xC1, 100, 100, 100, 0xFF])
                logging.info("sidePark adjust pos finish!")
                break
            else:
                # 映射Y轴到10~45速度，
                # round(100+error_x//10+4)
                serialObj.writeData([0xAA, 0xC1,100,round(100+error_x), 100, 0xFF])
                    
def identification(Cam,num,sign_index):
    global flag_slow
    global bad_target_num
    global fruit_2d_list
    global flag_target_left
    global flag_mystery
    person_index_list = [] # 定义一个列表存放四个result_index
    time_target0 = time.time()
    time_target1 = 0.1
    while True:
        try:
            if time_target1 < timeout_dict[sign_index]:  #超时长就放弃任务
                taskObj.testTask(num)
                if sign_index == 4 and taskObj.result_index != 0 and taskObj.height<100:
                    person_index_list.append(taskObj.result_index)
                    if len(person_index_list) == 4 and bad_target_num <2 and (not flag_mystery):
                        if numerous(person_index_list) not in [1,2]: # 列表中的众数不为坏人对应的index则break
                            logging.debug("identificating")
                            person_index_list = [] # 清空打坏人的列表
                            frontCamObj.clearDetectQueue() # 清空地标检测队列
                            cruiseObj.sendFlag.value = True 
                            Cam.pause()
                            serialObj.writeData([0xAA, 0xDE, 250, 100, 100, 0xFF])
                            flag_slow = True
                            flag_target_left = True
                            time.sleep(0.8)
                            # if flag_target_left: # 往左打
                            #     signDetectObj.resume()
                            signDetectObj.resume()
                            break
                    elif bad_target_num == 2:
                            frontCamObj.clearDetectQueue() # 清空地标检测队列
                            cruiseObj.sendFlag.value = True 
                            Cam.pause()
                            serialObj.writeData([0xAA, 0xDE, 250, 100, 100, 0xFF])
                            flag_slow = True
                            flag_target_left = True
                            time.sleep(0.8)
                            # if flag_target_left: # 往左打
                            #     signDetectObj.resume()
                            signDetectObj.resume()
                            break
                    
                elif sign_index == 7 and taskObj.result_index != 0:
                    fruit_2d_list.append([taskObj.result_index,taskObj.y])
                    
                # 防止城池和水果被误识别
                if ((sign_index == 1 and taskObj.result_index not in [1,2,4])
                    or (sign_index in [4,5,7] and taskObj.height > 80)):
                    error = -14
                # elif flag_mystery:
                #     error = taskObj.getXaxisError(num,sign_index,taskObj.y,flag_mystery)
                else:
                    error = taskObj.getXaxisError(num,sign_index,taskObj.y,flag_mystery)
                logging.debug("cam{} task error:{}".format(num,error))

                if sign_index == 3: # It's ridiculous!
                    break

                if error == 0:
                    # 发送停车指令给下位机
                    if sign_index == 4: 
                        serialObj.writeData([0xAA, 0xC2, 100, 100, 100, 0xFF]) # 骤停
                    else: 
                        serialObj.writeData([0xAA, 0xC1, 100, 100, 100, 0xFF]) # 有pid的停车
                    # time.sleep(0.05)
                    cruiseObj.pause() # 暂停巡航
                    frontCamObj.pause() # 关闭前摄像头
                    person_index_list = [] # 清空打坏人的列表
                    break
                else:
                    if sign_index == 3: # 夹小方块的时候走直线
                        serialObj.writeData([0xAA, 0xC1, 100+error, 100, 100, 0xFF])
                    else:
                        serialObj.writeData([0xAA, 0xC1, 100+error, 100, 100+cruiseObj.mid_error_share.value, 0xFF])
                time_target1 = time.time() - time_target0
            else:
                logging.info("Time is up, mission target not recognized")
                Cam.pause()
                serialObj.writeData([0xAA, 0xDE, 250, 100, 100, 0xFF])   #恢复速度
                cruiseObj.sendFlag.value = True 
                time.sleep(0.2)
                flag_slow = True
                frontCamObj.resume()
                signDetectObj.resume()
                break
        except Exception as e:
            logging.info(e)
            continue

def individualPurchase():
    global flag_slow
    right_target_x = rightTargetX[3]
    err_thresh = error_thresh[3]
    time_target0 = time.time()
    time_target1 = 0.1
    while True:
        try:
            if time_target1 < timeout_dict[3]:
                img = rightCamObj.readForDetect()
                detectX,detectY = purchasePreObj.detect(img.copy())
                error = detectX - right_target_x
                if abs(error) <= err_thresh:
                    error = 0
                if abs(error) <= 100 and error < 0:
                    error = -10
                elif abs(error) <= 100 and error > 0:
                    error = 10
                else:
                    error = error//10
                logging.debug("cam1 task error:{}".format(error))
                if error == 0:
                    # 发送停车指令给下位机
                    serialObj.writeData([0xAA, 0xC1, 100, 100, 100, 0xFF])
                    # time.sleep(0.05)
                    cruiseObj.pause() # 暂停巡航
                    frontCamObj.pause() # 暂时关闭前摄像头
                    break
                else:
                    serialObj.writeData([0xAA, 0xC1, 100+error, 100, 100, 0xFF])
                time_target1 = time.time() - time_target0
            else:
                logging.info("Time is up, mission target not recognized")
                rightCamObj.pause()
                serialObj.writeData([0xAA, 0xDE, 250, 100, 100, 0xFF])   #恢复速度
                cruiseObj.sendFlag.value = True 
                time.sleep(0.2)
                flag_slow = True
                frontCamObj.resume()
                signDetectObj.resume()
                break
        except Exception as e:
            logging.error(e)
            continue

def numerous(alist):
    #bincount()：统计非负整数的个数，不能统计浮点数
    counts = np.bincount(alist)
    return np.argmax(counts)


'''
description: 等待TC264发送AF
param {*} sign_index 地标下标
return {*}
'''
def waitingReceive_AF(sign_index):
    while True:  # 等待下位机完成任务
        logging.info("wait TC264 send finish task flag index{} in main".format(sign_index))
        if serialObj.readData() == 'af':
            logging.info("**********receive af at index{}**********".format(sign_index))
            break

"""
main loop
"""

if __name__ == "__main__":
    sign_index = 0
    sign_index_list = []
    start_time = time.time()
    stop_time = start_time + 1000 # 初始化地标前停车的时间戳
    logging.info("start_time:{}".format(start_time))
    stop_distance = 0
    Modules.Watch.Watch.Watcher() # 随时响应 Ctrl+c 的键盘输入
    while 1:
        if (signDetectObj.sign_y.value > sign_threshold["y_slow"] 
                and sign_threshold["x_lower"] < signDetectObj.sign_x.value < sign_threshold["x_upper"]
                and signDetectObj.height.value < sign_threshold["height_upper"]
                and sign_threshold["width_lower"] < signDetectObj.width.value < sign_threshold["width_upper"]):
            lock_sign.acquire()
            if signDetectObj.sign_index.value != 6 and signDetectObj.sign_index.value != 0:
                sign_index = signDetectObj.sign_index.value
                sign_y = signDetectObj.sign_y.value
            lock_sign.release()
            if sign_index in [1,2,3,4,5]:
                sign_index_list.append(sign_index)
                stop_distance = -0.1682*sign_y + sign_state[sign_index]["intercept"]
                delay_time = stop_distance / 59 # 150速度对应实际速度为172/2.9=59.3
                stop_time = time.time() + delay_time
                flag_slow = False
        if signDetectObj.sign_index.value == 6 and flag_slow:
            flag_slow = False
            serialObj.writeData([0xAA, 0xDE, 100+50, 100, 100, 0xFF])
            logging.info("send speed slow at seesaw")
        # 每种地标对应的任务只做一次;时间戳达到停车的时间;因为time.time一开始很大;地标检测仍开启(针对以物易物);跷跷板
        if (((not sign_state[sign_index]["accomplished"]) and time.time() - stop_time > 0 and (not flag_slow) and (not signDetectObj.getPausedFlag()))
            or (signDetectObj.sign_index.value == 6 and signDetectObj.sign_y.value > 160)):
            logging.info("time.time():{}".format(time.time()))
            logging.info("stop_time:{}".format(stop_time))
            logging.info("flag_slow:{}".format(flag_slow))
            logging.info("sign_index:{}".format(sign_index))
            logging.info("sign_index.value:{}".format(signDetectObj.sign_index.value))
            logging.info("stop_distance at sign{}:{}".format(sign_index,stop_distance))
            logging.info("sign_y when stop:{}".format((stop_distance-sign_state[sign_index]["intercept"])/(-0.1682)))
            stop_time = start_time + 1000 # 重置时间戳
            signDetectObj.pause()

        if signDetectObj.getPausedFlag():
            if len(sign_index_list) != 0:
                sign_index = numerous(sign_index_list) # 取众数
                if sign_state[sign_index]["accomplished"]:
                    while sign_index in sign_index_list:
                        sign_index_list.remove(sign_index)
                    sign_index = numerous(sign_index_list)
            sign_index_list = [] # 清空列表
            if sign_index in [1,2,3,4,5]: # 如果是跷跷板或者夹水果(index7)则不需要再次停车
                serialObj.writeData([0xAA, 0xDE, 100, 100, 100, 0xFF])
            if sign_index == 4: # 4是打坏人
                num_of_target = num_of_target + 1
                logging.info("num_of_target:{}".format(num_of_target))
                if (num_of_target == 1 or order_num == 2) and (not flag_2_mystery):
                    flag_mystery = True
                
            if signDetectObj.sign_index.value == 6:# 如果是跷跷板，则不开启侧边摄像头
                serialObj.writeData([0xAA, 0xDE, 150, 100, 100, 0xFF])
                time.sleep(2)
                flag_slow = True
                signDetectObj.resume()
                serialObj.writeData([0xAA, 0xDE, 250, 100, 100, 0xFF])
                time.sleep(0.1)
                continue
            
            logging.info("flag_target_left:{}".format(flag_target_left))
            logging.info("sign_index:{}".format(sign_index))
            logging.info("num_of_fortress:{}".format(num_of_fortress))
            if (sign_index in right_cam_sign) or (not flag_target_left) or (sign_index == 4 and num_of_fortress == 3): #开右摄像头
                if(num_of_target == 4):
                    num_of_target = 0
                rightCamObj.resume()
                time.sleep(0.4)
                # 巡航的偏差不再由巡航进程发送
                cruiseObj.sendFlag.value = False 
                taskObj = taskObj_dict[sign_index]
                taskObj.initData(1)
                identification(rightCamObj,1,sign_index)
                if sign_index == 3:
                    individualPurchase()
                # else:
                #     # 好坏人识别函数
                #     taskObj = taskObj_dict[sign_index]
                #     taskObj.initData(1)
                #     identification(rightCamObj,1,sign_index)

            else: # 开左摄像头
                leftCamObj.resume()
                time.sleep(0.4)
                # 巡航的偏差不再由巡航进程发送
                cruiseObj.sendFlag.value = False 
                taskObj = taskObj_dict[sign_index]
                taskObj.initData(2)
                ## 好坏人识别函数
                identification(leftCamObj,2,sign_index)

        # 发送对应任务指令给下位机，开始做任务！
        if cruiseObj.getPausedFlag():
            if sign_index == 1: # 城池
                serialObj.cleanInputBuffer()
                serialObj.writeData([0xAA, 0xAC, 1, taskObj.result_index, 0, 0xFF])
                num_of_fortress = num_of_fortress + 1
                order_num += 1
                logging.info("num_of_fortress:{}".format(num_of_fortress))
                waitingReceive_AF(sign_index)
                leftCamObj.pause()
                time.sleep(0.1)  # 延时一会，等待 left_cam 完全停止
                frontCamObj.resume()
                # time.sleep(1.0) # 让队列充满新图片，防止两次识别到地标(用前一张图片)
                if num_of_fortress % 3 == 0:
                    serialObj.writeData([0xAA, 0xDE, 200, 100, 100, 0xFF])
                else:
                    serialObj.writeData([0xAA, 0xDE, 250, 100, 100, 0xFF])
                cruiseObj.sendFlag.value = True 
                cruiseObj.resume()
                flag_slow = True
                sign_index = 0
                signDetectObj.resume()

            elif sign_index == 2: # 2表示侧方停车
                serialObj.cleanInputBuffer()
                # 向左侧方停车
                sideParking(IS_FRIENDSHIP_OPENLOOP)
                time.sleep(0.4)
                # 20代表2.0秒，15代表1.5秒，这个是侧方停车右移出来的时间，需要根据实际情况调整
                serialObj.writeData([0xAA, 0xAC, 2, 20, 0, 0xFF])
                waitingReceive_AF(sign_index)
                serialObj.writeData([0xAA, 0xC1, 100, 100+100, 100, 0xFF])
                logging.info("friendship openloop right move")
                # 延时3秒
                time.sleep(FRIENDSHIP_MOVETIME)
                serialObj.writeData([0xAA, 0xC1, 100, 100, 100, 0xFF])
                leftCamObj.pause()
                time.sleep(0.1)  # 延时一会，等待 left_cam 完全停止
                frontCamObj.resume()
                # time.sleep(1.0) # 让队列充满新图片，防止两次识别到地标(用前一张图片)
                serialObj.writeData([0xAA, 0xDE, 250, 100, 100, 0xFF])
                cruiseObj.sendFlag.value = True 
                cruiseObj.resume()
                flag_slow = True
                signDetectObj.resume()
                sign_state[sign_index]["accomplished"] = True # 更新此任务的完成情况
                sign_index = 0 # 一定要在更新sign_state之后

            elif sign_index == 3: # 3表示purchase
                serialObj.cleanInputBuffer()
                serialObj.writeData([0xAA, 0xAC, 3, 9+100, 0, 0xFF])
                order_num += 1
                time.sleep(0.1)
                waitingReceive_AF(sign_index)
                rightCamObj.pause()
                time.sleep(0.1)  # 延时一会，等待 right_cam 完全停止
                frontCamObj.resume()
                # time.sleep(1.0) # 让队列充满新图片，防止两次识别到地标(用前一张图片)
                serialObj.writeData([0xAA, 0xDE, 250, 100, 100, 0xFF])
                cruiseObj.sendFlag.value = True 
                cruiseObj.resume()
                flag_slow = True
                signDetectObj.resume()
                sign_state[sign_index]["accomplished"] = True # 更新此任务的完成情况
                sign_index = 0

            elif sign_index == 4: # 4表示打坏人
                serialObj.cleanInputBuffer()
                logging.info("person result index:{}".format(taskObj.result_index))
                if taskObj.result_index in [1, 2]: # 如果识别到坏人则吹风扇，否则恢复巡航
                    bad_target_num += 1
                    area_div_y = int((taskObj.height) * (taskObj.width) / (taskObj.y))
                    if area_div_y > 45:
                        target_dist = -0.265*(taskObj.y) + 42 + 2
                    elif 18 < area_div_y <= 45:
                        target_dist = -0.211*(taskObj.y) + 47 + 2
                    else:
                        target_dist = -0.195*(taskObj.y) + 52 + 2
                    target_dist = min(target_dist, 34)
                    if (not flag_target_left) or (num_of_fortress == 3): # 往右打
                        serialObj.writeData([0xAA, 0xAC, 4, round(target_dist)+100, 0, 0xFF])
                    else: # 往左打
                        serialObj.writeData([0xAA, 0xAC, 4, (-1)*round(target_dist)+100, 0, 0xFF])
                    logging.info("area_div_y:{}".format(area_div_y))
                    logging.info("taskObj.y at index4:{}".format(taskObj.y))
                    logging.info("target_dist:{}".format(round(target_dist)))
                    waitingReceive_AF(sign_index)
                if (not flag_target_left) or (num_of_fortress == 3): 
                    rightCamObj.pause()
                else:
                    leftCamObj.pause()
                
                if flag_mystery:
                    serialObj.writeData([0xAA, 0xAC, 8, 110, 200, 0xFF]) # 神秘任务指令
                    waitingReceive_AF(8)
                    flag_mystery = False
                    flag_2_mystery = True
                    
                time.sleep(0.1)  # 延时一会，等待 left_cam 完全停止
                frontCamObj.resume()
                # time.sleep(1.0) # 让队列充满新图片，防止两次识别到地标(用前一张图片)
                serialObj.writeData([0xAA, 0xDE, 250, 100, 100, 0xFF])
                cruiseObj.sendFlag.value = True
                cruiseObj.resume()
                flag_slow = True
                flag_target_left = True
                sign_index = 0
                signDetectObj.resume()
                

            elif sign_index == 5: # 5表示trade,放小方块
                serialObj.cleanInputBuffer()
                serialObj.writeData([0xAA, 0xAC, 5, 120, 120, 0xFF])
                waitingReceive_AF(sign_index)
                sign_index = 7
                signDetectObj.sign_index.value = 7
                frontCamObj.resume()
                cruiseObj.resume() # 为了下一步夹水果任务的调整有巡航
                continue # 因为此时sign_index= 7，所以得先跳过下面这个elif（回到上面的位置调整函数）

            elif sign_index == 7: # 7表示trade,夹水果
                serialObj.cleanInputBuffer()
                fruit_y_list = []
                fruit_index_array = np.array(fruit_2d_list)[:,0]
                fruit_index = numerous(fruit_index_array)
                for i in fruit_2d_list:
                    if i[0] == fruit_index:
                        fruit_y_list.append(i[1])
                fruit_y = int(np.mean(fruit_y_list))
                if (fruit_y < fruit_y_thresh and fruit_index in [1, 6]) or (fruit_y >= fruit_y_thresh and fruit_index not in [1, 6]):
                    serialObj.writeData([0xAA, 0xAC, 7, 135, 210, 0xFF]) # 如果上方是1或6，则上升高度为210（上层）
                else:
                    serialObj.writeData([0xAA, 0xAC, 7, 135, 120, 0xFF]) # 否则上升高度为120（下层）
                logging.info("fruit_y at index7:{}".format(fruit_y))
                logging.info("fruit_index at index7:{}".format(fruit_index))
                waitingReceive_AF(sign_index)
                rightCamObj.pause()
                time.sleep(0.1)  # 延时一会，等待 left_cam 完全停止
                frontCamObj.resume()
                # time.sleep(1.0) # 让队列充满新图片，防止两次识别到地标(用前一张图片)
                serialObj.writeData([0xAA, 0xDE, 250, 100, 100, 0xFF])
                cruiseObj.sendFlag.value = True
                cruiseObj.resume()
                flag_slow = True
                sign_index = 0
                signDetectObj.resume()
                flag_target_left = False # 让下一次打坏人开右摄像头

