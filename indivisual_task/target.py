#!/usr/bin/python
# coding=UTF-8
import os
import sys
sys.path.append("/home/root/workspace/17BaiduAI_V4") 
import time
from Modules.Config import logging
from Modules.Config import IS_FRIENDSHIP_OPENLOOP
from Modules.Config import timeout_dict
from Modules.Cruise.CruiseCpp import *
from Modules.Vision.ModelTest import *
from Modules.Vision.Preprocess import sideParkObj
from Modules.Serial.Serial import serialObj   
import Modules.Watch.Watch

"""
camera cruise detect 开启进程
"""

frontCamObj.start()
cruiseObj.start()
signDetectObj.start()
leftCamObj.start()
time.sleep(0.01)
leftCamObj.pause()
rightCamObj.start()
time.sleep(0.01)
rightCamObj.pause()
"""
全局变量
"""
# friend_index = 0  #友谊放歌标号，识别后置一不再进入该模型
flag_slow = True      # 用于防止多次进入发送在地标前减速的if里面
flag_target_left = True #
# left_cam_sign = [1,2] # 记录需要开启左摄像头的地标index
# right_cam_sign= [3,5,7] # 记录需要开启右摄像头的地标index
left_cam_target = [1,2,3] # 记录需要开启左摄像头的地标index
right_cam_target= [4] # 记录需要开启右摄像头的地标index
num_of_target = 0       # 记录现在是第几个打坏人 
# left_cam_num = [3,4,6] # 第3,4,6次地标的时候是左摄像头
taskObj_dict = {1:taskDetectObj, 
                2:taskDetectObj, 
                3:taskDetectObj,
                4:taskPersonDetectObj, 
                5:taskFruitDetectObj, 
                6:taskFruitDetectObj,
                7:taskFruitDetectObj} # 根据地标index选择对应的模型

def identification(Cam,num,sign_index):
    global flag_slow
    global num_of_target
    global flag_target_left
    result_index_list = [] # 定义一个列表存放五个result_index
    time_target0 = time.time()
    time_target1 = 0.1
    while True:
            try:
                if time_target1<= timeout_dict[sign_index]:   #定时3秒未识别到目标（该任务已完成）就继续巡航
                    taskObj.testTask(num)
                    if sign_index == 4 and taskObj.result_index != 0 and taskObj.height<100:
                        result_index_list.append(taskObj.result_index)
                        if len(result_index_list) == 10:
                            #bincount（）：统计非负整数的个数，不能统计浮点数
                            counts = np.bincount(result_index_list)
                            if np.argmax(counts) not in [1,2]: # 列表中的众数不为坏人对应的index则break
                                result_index_list = [] # 清空打坏人的列表
                                frontCamObj.pause() # 暂时关闭前摄像头
                                cruiseObj.sendFlag.value = True 
                                Cam.pause()
                                serialObj.writeData([0xAA, 0xDE, 200, 100, 100, 0xFF])
                                flag_slow = True
                                frontCamObj.resume()
                                time.sleep(0.8)
                                signDetectObj.resume()
                                if num_of_target == 3:
                                    flag_target_left = False
                                    logging.info("flag_target_left:{}".format(flag_target_left))
                                break
                    error = taskObj.getXaxisError(num,sign_index,taskObj.y)
                    logging.debug("cam{} task error:{}".format(num,error))
                    if error == 0:
                        # 发送停车指令给下位机
                        serialObj.writeData([0xAA, 0xC1, 100, 100, 100, 0xFF])
                        time.sleep(0.05)
                        cruiseObj.pause() # 暂停巡航
                        frontCamObj.pause() # 暂时关闭前摄像头
                        result_index_list = [] # 清空打坏人的列表
                        break
                    else:
                        serialObj.writeData([0xAA, 0xC1, 100+error, 100, 100+cruiseObj.mid_error_share.value, 0xFF])
                        # +cruiseObj.mid_error_share.value
                    time_target1 = time.time() - time_target0
                else:   #3秒未识别到目标，关闭侧边摄像头，恢复速度，继续巡航
                    logging.info("时间已到，未识别到任务目标")
                    if num_of_target == 3:
                        flag_target_left = False
                        logging.info("flag_target_left:{}".format(flag_target_left))
                    cruiseObj.sendFlag.value = True 
                    Cam.pause()
                    serialObj.writeData([0xAA, 0xDE, 200, 100, 100, 0xFF])
                    flag_slow = True
                    frontCamObj.resume()
                    signDetectObj.resume()
                    break
            except Exception as e:
                logging.info(e)
                continue

"""
main loop
"""
if __name__ == "__main__":
    sign_index = 0
    Modules.Watch.Watch.Watcher() # 随时响应 Ctrl+c 的键盘输入
    while 1:
        if (signDetectObj.sign_y.value > sign_threshold["y_slow"] 
                and sign_threshold["x_lower"] < signDetectObj.sign_x.value < sign_threshold["x_upper"]
                and flag_slow and signDetectObj.height.value < sign_threshold["height_lower"]
                and (signDetectObj.sign_index.value == 4 or signDetectObj.sign_index.value ==6)):
            flag_slow = False
            # 设置下位机targetPos为20，减速
            serialObj.writeData([0xAA, 0xDE, 100+20, 100, 100, 0xFF])
            logging.info("send speed slow")
            # signDetectObj.resume()
    
        if signDetectObj.getPausedFlag():
            sign_index = signDetectObj.sign_index.value
            if sign_index == 4: # 4是打坏人
                serialObj.writeData([0xAA, 0xDE, 100, 100, 100, 0xFF])
                num_of_target = num_of_target + 1
                logging.info("num_of_target:{}".format(num_of_target))
                    
                if num_of_target in right_cam_target: #开右摄像头
                    if(num_of_target == 4):
                        num_of_target = 0
                    rightCamObj.resume()
                    time.sleep(0.4)
                    # 巡航的偏差不再由巡航进程发送
                    cruiseObj.sendFlag.value = False 
                    taskObj = taskObj_dict[sign_index]
                    taskObj.initData(1)
                    # 好坏人识别函数
                    identification(rightCamObj,1,sign_index)
                    
                else : # 开左摄像头
                    leftCamObj.resume()
                    time.sleep(0.4)
                    # 巡航的偏差不再由巡航进程发送
                    cruiseObj.sendFlag.value = False 
                    taskObj = taskObj_dict[sign_index]
                    taskObj.initData(2)
                    ## 好坏人识别函数
                    identification(leftCamObj,2,sign_index)
            elif sign_index  == 6:# 如果是跷跷板，则不开启侧边摄像头
                serialObj.writeData([0xAA, 0xDE, 130, 100, 100, 0xFF])
                time.sleep(2)
                flag_slow = True
                signDetectObj.resume()
                serialObj.writeData([0xAA, 0xDE, 200, 100, 100, 0xFF])
                continue
    
            else :
                signDetectObj.resume()
            
        if cruiseObj.getPausedFlag() and sign_index == 4: # 4表示打坏人
            if taskObj.result_index in [1,2]: # 如果识别到坏人则吹风扇，否则恢复巡航
                area_div_y = int((taskObj.height) * (taskObj.width) / (taskObj.y))
                if area_div_y > 45:
                    target_dist = -0.265*(taskObj.y) + 42 + 2
                elif 18 < area_div_y <= 45:
                    target_dist = -0.211*(taskObj.y) + 47 + 2
                else:
                    target_dist = -0.195*(taskObj.y) + 52 + 2
                if target_dist > 34:
                    target_dist = 34
                if flag_target_left: # 往左打
                    serialObj.cleanInputBuffer()   #清空缓存
                    serialObj.writeData([0xAA, 0xAC, 4, (-1)*round(target_dist)+100, 0, 0xFF]) # TODO:需要加打坏人的距离
                else: # 往右打
                    serialObj.cleanInputBuffer()
                    serialObj.writeData([0xAA, 0xAC, 4, round(target_dist)+100, 0, 0xFF])
                logging.info("area_div_y:{}".format(area_div_y))
                logging.info("taskObj.y at index4:{}".format(taskObj.y))
                logging.info("target_dist:{}".format(round(target_dist)))
                while True:  # 等待下位机完成任务
                    logging.info('wait TC264 send finish task flag index4 in main')
                    if serialObj.readData() == 'af':
                        logging.info('**********receive af at index4**********')
                        break
            if flag_target_left: # 往左打
                leftCamObj.pause()
            else:
                rightCamObj.pause()
            time.sleep(0.1)  # 延时一会，等待 left_cam 完全停止
            frontCamObj.resume()
            # time.sleep(1.0) # 让队列充满新图片，防止两次识别到地标(用前一张图片)
            serialObj.writeData([0xAA, 0xDE, 200, 100, 100, 0xFF])
            cruiseObj.sendFlag.value = True 
            cruiseObj.resume()
            flag_slow = True
            signDetectObj.resume()
            if num_of_target == 3:
                flag_target_left = False
                logging.info("flag_target_left:{}".format(flag_target_left))
            else:
                pass
