<!--
 * @Author: your name
 * @Date: 2021-08-16 19:20:00
 * @LastEditTime: 2022-04-28 16:57:49
 * @LastEditors: your name
 * @Description: In User Settings Edit
 * @FilePath: \17th_BAIDUAI_V1\README.md
-->
# 17th_BAIDU_AI

![](https://img.shields.io/badge/Version-v0.0.1-brightgreen)        ![](https://img.shields.io/badge/Platform-linux,windows-orange)        ![](https://img.shields.io/badge/Building-passing-brightgreen)       

## 简介
V4在V3基础上色块使用模型识别一次后使用opencv识别定位，侧方停车右移使用上位机延时方案。
## 运行

```python
python3 main.py
```

## 常用指令

`lsusb`:查看usb接了什么设备

`lsusb --t`:查看usb设备，以树状图呈现

`ls /dev/video*`查看摄像头

## 代码规范
文件、文件夹名称：大驼峰 例如：Vision,HandDetector.py

类名：大驼峰+Class 例如：CameraClass

对象名：小驼峰+Obj 例如：cameraObj

类内私有变量：__+小写 例如：self.__num

函数名称：小驼峰 例如：def takePicture()

普通变量：小写，以下划线隔开 例如：img_path

全局变量：g+小写，以下划线隔开 例如：g_img_path




[驼峰命名法](https://www.jianshu.com/p/de0016754d8a)

## 日志

