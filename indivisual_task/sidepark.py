#!/usr/bin/python
# coding=UTF-8
import os
import sys
sys.path.append("/home/root/workspace/17BaiduAI_V4") 
import time
from Modules.Config import logging
from Modules.Config import IS_FRIENDSHIP_OPENLOOP
from Modules.Config import timeout_dict
from Modules.Cruise.CruiseCpp import *
from Modules.Vision.ModelTest import *
from Modules.Vision.Preprocess import sideParkObj
from Modules.Serial.Serial import serialObj   
import Modules.Watch.Watch

"""
camera cruise detect 开启进程
"""

frontCamObj.start()
cruiseObj.start()
signDetectObj.start()
leftCamObj.start()
time.sleep(0.01)
leftCamObj.pause()
rightCamObj.start()
time.sleep(0.01)
rightCamObj.pause()
"""
全局变量
"""
flag_slow = True      # 用于防止多次进入发送在地标前减速的if里面
flag_target_left = True
taskObj_dict = {1:taskDetectObj, 
                2:taskDetectObj, 
                3:taskDetectObj,
                4:taskPersonDetectObj, 
                5:taskFruitDetectObj, 
                6:taskFruitDetectObj,
                7:taskFruitDetectObj} # 根据地标index选择对应的模型

def sideParking(is_friendship_openloop = 1):
    if is_friendship_openloop == 1:
        # 20的速度，向左平移
        serialObj.writeData([0xAA, 0xC1, 100, 100-30, 100, 0xFF])
        logging.info("friendship openloop")
        # 延时3秒
        time.sleep(3)
        serialObj.writeData([0xAA, 0xC1, 100, 100, 100, 0xFF])
    else:
        frontCamObj.resume()
        time.sleep(0.4)
        target_pos =(80,60)
        last_pos = (0,0)
        curr_pos = (0,0)
        while True:
            img = frontCamObj.readForDetect()
            if img is not None:
                pos_res = sideParkObj.detect(img.copy())
                logging.info("sidePark_pos_res:{}".format(pos_res))
                if(pos_res == (0,0)):
                    curr_pos = last_pos
                else:
                    curr_pos = pos_res
                    last_pos = curr_pos
                error_x = curr_pos[0] - target_pos[0]
                error_y = curr_pos[1] - target_pos[1]
                # 限制范围
                if(abs(error_x)<=2):
                    error_x = 0
                if(abs(error_y)<=10):
                    error_y = 0
                if(error_x==0 ):
                    serialObj.writeData([0xAA, 0xC1, 100, 100, 100, 0xFF])
                    logging.info("sidePark adjust pos finish!")
                    break
                else:
                    # 映射Y轴到10~45速度，X轴映射到5~10速度
                    # round(100+error_x//10+4)
                    if error_x > 0:
                        serialObj.writeData([0xAA, 0xC1,100,round(100+error_x*0.4+13), 100, 0xFF])
                    elif error_x < 0:
                        serialObj.writeData([0xAA, 0xC1,100,round(100+error_x*0.4-13), 100, 0xFF])
                        
def identification(Cam,num,sign_index):
    global flag_slow
    global friend_index
    result_index_list = [] # 定义一个列表存放五个result_index
    while True:
            try:
                taskObj.testTask(num)
                if sign_index == 4 and taskObj.result_index != 0 and taskObj.height<100:
                    result_index_list.append(taskObj.result_index)
                    if len(result_index_list) == 10:
                        #bincount（）：统计非负整数的个数，不能统计浮点数
                        counts = np.bincount(result_index_list)
                        if np.argmax(counts) not in [1,2]: # 列表中的众数不为坏人对应的index则break
                            result_index_list = [] # 清空打坏人的列表
                            frontCamObj.pause() # 暂时关闭前摄像头
                            cruiseObj.sendFlag.value = True 
                            Cam.pause()
                            serialObj.writeData([0xAA, 0xDE, 200, 100, 100, 0xFF])
                            flag_slow = True
                            frontCamObj.resume()
                            time.sleep(0.8)
                            if flag_target_left: # 往左打
                                signDetectObj.resume()
                            break

                error = taskObj.getXaxisError(num,sign_index,taskObj.y)
                logging.debug("cam{} task error:{}".format(num,error))
                
                if error == 0:
                    # 发送停车指令给下位机
                    serialObj.writeData([0xAA, 0xC1, 100, 100, 100, 0xFF])
                    time.sleep(0.05)
                    cruiseObj.pause() # 暂停巡航
                    frontCamObj.pause() # 暂时关闭前摄像头
                    result_index_list = [] # 清空打坏人的列表
                    break
                else:
                    serialObj.writeData([0xAA, 0xC1, 100+error, 100, 100+cruiseObj.mid_error_share.value, 0xFF])
                    # +cruiseObj.mid_error_share.value
            except Exception as e:
                logging.info(e)
                continue

"""
main loop
"""
if __name__ == "__main__":
    sign_index = 0
    Modules.Watch.Watch.Watcher() # 随时响应 Ctrl+c 的键盘输入
    while 1:
        if (signDetectObj.sign_y.value > sign_threshold["y_slow"] 
                and sign_threshold["x_lower"] < signDetectObj.sign_x.value < sign_threshold["x_upper"]
                and flag_slow and signDetectObj.height.value < sign_threshold["height_lower"] 
                and (signDetectObj.sign_index.value == 2 or signDetectObj.sign_index.value == 6)):
            flag_slow = False
            # 设置下位机targetPos为20，减速
            serialObj.writeData([0xAA, 0xDE, 100+20, 100, 100, 0xFF])
            logging.info("send speed slow")
            # signDetectObj.resume()

        if signDetectObj.getPausedFlag():
            sign_index = signDetectObj.sign_index.value
            if sign_index  == 6:# 如果是跷跷板，则不开启侧边摄像头
                serialObj.writeData([0xAA, 0xDE, 130, 100, 100, 0xFF])
                time.sleep(2)
                flag_slow = True
                signDetectObj.resume()
                serialObj.writeData([0xAA, 0xDE, 200, 100, 100, 0xFF])
                continue
            elif sign_index == 2:
                serialObj.writeData([0xAA, 0xDE, 100, 100, 100, 0xFF])
                # 开左摄像头
                leftCamObj.resume()
                time.sleep(0.4)
                # 巡航的偏差不再由巡航进程发送
                cruiseObj.sendFlag.value = False 
                taskObj = taskObj_dict[sign_index]
                taskObj.initData(2)
                ## 好坏人识别函数
                identification(leftCamObj,2,sign_index)
            else :
                signDetectObj.resume()

        if cruiseObj.getPausedFlag() and sign_index == 2 : # 2表示侧方停车
            # 向左侧方停车
            sideParking(IS_FRIENDSHIP_OPENLOOP)
            time.sleep(0.4)
            serialObj.cleanInputBuffer()
            serialObj.writeData([0xAA, 0xAC, 2, 15, 0, 0xFF])
            while True:  # 等待下位机完成任务
                logging.info('wait TC264 send finish task flag index2 in main')
                if serialObj.readData() == 'af':
                    logging.info('**********receive af at index2**********')
                    break
            leftCamObj.pause()
            time.sleep(0.1)  # 延时一会，等待 left_cam 完全停止
            frontCamObj.resume()
            # time.sleep(1.0) # 让队列充满新图片，防止两次识别到地标(用前一张图片)
            serialObj.writeData([0xAA, 0xDE, 200, 100, 100, 0xFF])
            cruiseObj.sendFlag.value = True 
            cruiseObj.resume()
            flag_slow = True
            signDetectObj.resume()

       