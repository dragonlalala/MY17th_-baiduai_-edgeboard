#!/usr/bin/python
# coding=UTF-8
import os
import sys
sys.path.append("/home/root/workspace/17BaiduAI_V4") 
import time
from Modules.Config import logging
from Modules.Config import IS_FRIENDSHIP_OPENLOOP
from Modules.Config import timeout_dict
from Modules.Cruise.CruiseCpp import *
from Modules.Vision.ModelTest import *
from Modules.Vision.Preprocess import sideParkObj,purchasePreObj
from Modules.Serial.Serial import serialObj   
import Modules.Watch.Watch

"""
camera cruise detect 开启进程
"""

frontCamObj.start()
cruiseObj.start()
signDetectObj.start()
leftCamObj.start()
time.sleep(0.01)
leftCamObj.pause()
rightCamObj.start()
time.sleep(0.01)
rightCamObj.pause()
"""
全局变量
"""
# bad_target_num = 0  #友谊放歌标号，识别后置一不再进入该模型
flag_slow = True      # 用于防止多次进入发送在地标前减速的if里面
# flag_target_left = True #
# left_cam_sign = [1,2] # 记录需要开启左摄像头的地标index
# right_cam_sign= [3,5,7] # 记录需要开启右摄像头的地标index
# num_of_target = 0       # 记录现在是第几个打坏人 
# left_cam_num = [3,4,6] # 第3,4,6次地标的时候是左摄像头
fruit_2d_list = [] # 用于存放水果index和y坐标的二维列表
taskObj_dict = {1:taskDetectObj, 
                2:taskDetectObj, 
                3:taskDetectObj, # taskPurchaseDetectObj
                4:taskPersonDetectObj, 
                5:taskFruitDetectObj, 
                6:taskFruitDetectObj,
                7:taskFruitDetectObj} # 根据地标index选择对应的模型

def identification(Cam,num,sign_index):
    global flag_slow
    global bad_target_num
    global fruit_2d_list
    result_index_list = [] # 定义一个列表存放五个result_index
    time_target0 = time.time()
    time_target1 = 0.1
    while True:
        try:
            if time_target1 < timeout_dict[sign_index]:  #超时长就放弃任务
                taskObj.testTask(num)
                if sign_index == 7 and taskObj.result_index != 0:
                    fruit_2d_list.append([taskObj.result_index,taskObj.y])
                if sign_index in [5,7] and taskObj.height > 80:
                    error = -14
                else:
                    error = taskObj.getXaxisError(num,sign_index,taskObj.y)
                logging.debug("cam{} task error:{}".format(num,error))
                
                if error == 0:
                    # 发送停车指令给下位机
                    serialObj.writeData([0xAA, 0xC1, 100, 100, 100, 0xFF])
                    time.sleep(0.05)
                    cruiseObj.pause() # 暂停巡航
                    frontCamObj.pause() # 暂时关闭前摄像头
                    result_index_list = [] # 清空打坏人的列表
                    break
                else:
                    serialObj.writeData([0xAA, 0xC1, 100+error, 100, 100+cruiseObj.mid_error_share.value, 0xFF])
                time_target1 = time.time() - time_target0
            else:
                logging.info("Time is up, mission target not recognized")
                Cam.pause()
                serialObj.writeData([0xAA, 0xDE, 200, 100, 100, 0xFF])   #恢复速度
                cruiseObj.sendFlag.value = True 
                time.sleep(0.2)
                flag_slow = True
                frontCamObj.resume()
                signDetectObj.resume()
                break
        except Exception as e:
            logging.info(e)
            continue

'''
description: 等待TC264发送AF
param {*} sign_index 地标下标
return {*}
'''
def waitingReceive_AF(sign_index):
    while True:  # 等待下位机完成任务
        logging.info("wait TC264 send finish task flag index{} in main".format(sign_index))
        if serialObj.readData() == 'af':
            logging.info("**********receive af at index{}**********".format(sign_index))
            break

"""
main loop
"""

if __name__ == "__main__":
    sign_index = 0
    Modules.Watch.Watch.Watcher() # 随时响应 Ctrl+c 的键盘输入
    while 1:
        if (signDetectObj.sign_y.value > sign_threshold["y_slow"] 
                and sign_threshold["x_lower"] < signDetectObj.sign_x.value < sign_threshold["x_upper"]
                and flag_slow and signDetectObj.height.value < sign_threshold["height_lower"]):
            if signDetectObj.sign_index.value == 5:
                flag_slow = False
                # 设置下位机targetPos为20，减速
                serialObj.writeData([0xAA, 0xDE, 100+20, 100, 100, 0xFF])
                logging.info("send speed slow")
            elif signDetectObj.sign_index.value == 6:
                flag_slow = False
                serialObj.writeData([0xAA, 0xDE, 100+50, 100, 100, 0xFF])
                logging.info("send speed slow at seesaw")
            # signDetectObj.resume()

        if signDetectObj.getPausedFlag():
            sign_index = signDetectObj.sign_index.value
            if sign_index in [1,2,3,4] : # 4是打坏人
                signDetectObj.resume()
                continue
            serialObj.writeData([0xAA, 0xDE, 100, 100, 100, 0xFF])
            
            if sign_index  == 6:# 如果是跷跷板，则不开启侧边摄像头
                serialObj.writeData([0xAA, 0xDE, 130, 100, 100, 0xFF])
                time.sleep(2)
                flag_slow = True
                signDetectObj.resume()
                serialObj.writeData([0xAA, 0xDE, 200, 100, 100, 0xFF])
                continue

            else: #开右摄像头
                rightCamObj.resume()
                time.sleep(0.4)
                # 巡航的偏差不再由巡航进程发送
                cruiseObj.sendFlag.value = False 
                # 好坏人识别函数
                taskObj = taskObj_dict[7]
                taskObj.initData(1)
                identification(rightCamObj,1,7)

        # 发送对应任务指令给下位机，开始做任务！
        if cruiseObj.getPausedFlag():
            serialObj.cleanInputBuffer()
            fruit_y_list = []
            fruit_index_array = np.array(fruit_2d_list)[:,0]
            counts = np.bincount(fruit_index_array)
            fruit_index = np.argmax(counts)
            for i in fruit_2d_list:
                if i[0] == fruit_index:
                    fruit_y_list.append(i[1])
            fruit_y = int(np.mean(fruit_y_list))
            if (fruit_y < fruit_y_thresh and fruit_index in [1, 6]) or (fruit_y >= fruit_y_thresh and fruit_index not in [1, 6]):
                serialObj.writeData([0xAA, 0xAC, 7, 130, 210, 0xFF]) # 如果上方是1或6，则上升高度为210（上层）
            else:
                serialObj.writeData([0xAA, 0xAC, 7, 130, 120, 0xFF]) # 否则上升高度为120（下层）
            logging.info("fruit_y at index7:{}".format(fruit_y))
            logging.info("fruit_index at index7:{}".format(fruit_index))
            waitingReceive_AF(sign_index)
            rightCamObj.pause()
            time.sleep(0.1)  # 延时一会，等待 left_cam 完全停止
            frontCamObj.resume()
            # time.sleep(1.0) # 让队列充满新图片，防止两次识别到地标(用前一张图片)
            serialObj.writeData([0xAA, 0xDE, 200, 100, 100, 0xFF])
            cruiseObj.sendFlag.value = True
            cruiseObj.resume()
            flag_slow = True
            signDetectObj.resume()
            # flag_target_left = False # 让下一次打坏人开右摄像头
